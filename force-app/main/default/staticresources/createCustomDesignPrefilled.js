({
 createDesign: function (component) {
        var createRecordEvent = $A.get('e.force:createRecord');
        if ( createRecordEvent ) {
            {!REQUIRESCRIPT("https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js")}
            try {
                var query = "SELECT Id, Status__c, Panel_Count__c, Panel_Type__c,"
                	+ " System_Size__c, System_Cost__c, Soft_Roof_Quote__c,"
                    + " Number_of_Steep_Roofs__c, Number_of_Flat_Roofs__c,"
                    + " Number_of_Multi_Array_Panels__c, Aurora_Production__c"
                    + " Opportunity__c, RecordTypeId, Created_Date__c"
                	+ " FROM Design__c"
                	+ " WHERE RecordTypeId = '012290000001ejrAAA'"
                	+ " ORDER BY Created_Date__c DESC LIMIT 2";
                var records = sforce.connection.query(query);
                var designRecords = records.getArray('records');
                alert(records);
                if(designRecords.length == 0){
					alert('There is no Designs');
                    createRecordEvent.setParams({
                	'entityApiName': 'Design__c',
                	'defaultFieldValues': {
                    	'Status__c' : null,
                    	'Panel_Count__c' : null,
                    	'Panel_Type__c' : null,
                    	'System_Size__c' : null,
                    	'System_Cost__c' : null,
                    	'Soft_Roof_Quote__c' : null,
                    	'Number_of_Steep_Roofs__c' : null,
                    	'Number_of_Flat_Roofs__c' : null,
                    	'Number_of_Multi_Array_Panels__c' : null,
	                    'Aurora_Production__c' : null,
                    	'Opportunity__c' : null 
                	}
            		});
                } else { 
                    createRecordEvent.setParams({
                	'entityApiName': 'Design__c',
                	'defaultFieldValues': {
                    	'Status__c' : designRecords.Status__c,
                    	'Panel_Count__c' : designRecords.Panel_Count__c,
                    	'Panel_Type__c' : designRecords.Panel_Type__c,
                    	'System_Size__c' : designRecords.System_Size__c,
                    	'System_Cost__c' : designRecords.System_Cost__c,
                    	'Soft_Roof_Quote__c' : designRecords.Soft_Roof_Quote__c,
                    	'Number_of_Steep_Roofs__c' : designRecords.Number_of_Steep_Roofs__c,
                    	'Number_of_Flat_Roofs__c' : designRecords.Number_of_Flat_Roofs__c,
                    	'Number_of_Multi_Array_Panels__c' : designRecords.Number_of_Multi_Array_Panels__c,
	                    'Aurora_Production__c' : designRecords.Aurora_Production__c,
                    	'Opportunity__c' : designRecords.Opportunity__c 
                	}
            		});
                }
            	createRecordEvent.fire();
            } catch(e) {
				alert('An Error has Occured. Error:' +e);
            }
            
        } else {
            /* Create Design Record Event is not supported */
            alert("Design creation not supported");
        }
    }
})