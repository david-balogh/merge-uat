@isTest
public class TestPMEventController {
	static testMethod void testGoToLead() {
    	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.EventPagePM;
        // create lead 
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
        //create event and link
      	Event evToCreate = new  Event();
        evToCreate.Subject = 'TEST';
        evToCreate.OwnerId = userToCreate.id;
        evToCreate.Location = '463 Lafayette Ave, Brookly, NY, 11205';
        evToCreate.Lead__c = leadToCreate.Id;
        evToCreate.StartDateTime = DateTime.now();
        evToCreate.EndDateTime = DateTime.now() + 1;
        insert evToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', evToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(evToCreate);
        pmEventController myContr = new pmEventController(sc);
    	myContr.goToLead();
    	Test.stopTest();
   }
    
   static testMethod void testGoToOpp() {
    	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.EventPagePM;
       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        
        //create event and link
      	Event evToCreate = new  Event();
        evToCreate.Subject = 'TEST';
        evToCreate.OwnerId = userToCreate.id;
        evToCreate.Location = '463 Lafayette Ave, Brookly, NY, 11205';
        evToCreate.Opportunity__c = oppToCreate.Id;
        evToCreate.StartDateTime = DateTime.now();
        evToCreate.EndDateTime = DateTime.now() + 1;
        insert evToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', evToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(evToCreate);
        pmEventController myContr = new pmEventController(sc);
    	myContr.goToOpportunity();
    	Test.stopTest();
   }
}