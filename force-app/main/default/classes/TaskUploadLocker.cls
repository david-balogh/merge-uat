public class TaskUploadLocker {
    
   Task_Upload__c [] custObj = [SELECT Id, Status__c from Task_Upload__c WHERE Status__c LIKE 'Completed'];

   Approval.LockResult[] lrList = Approval.lock(custObj, false); // Locking the record

}