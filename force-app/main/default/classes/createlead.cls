public with sharing class createlead {

   
    public lead l {get;set;}
    
    public createlead(ApexPages.StandardController controller) {
    // blank constructor
    l =new Lead();

  }
 
      // save button is clicked
      public PageReference save() {
          try{
            insert l ;
              
            apexpages.addMessage(new apexpages.Message (Apexpages.severity.confirm, 'lead created ' ));
            // l = new Lead();
            // return Apexpages.currentPage();
            
            // This example works on desktop for sending user to newly created lead, it also makes the book appointment form pop up.  
            // PageReference reRend = new PageReference('https://nystatesolar--build.lightning.force.com/lightning/action/quick/Global.FSL__Book_Appointment?objectApiName&recordId='+l.id+'&backgroundContext=%2Flightning%2Fr%2FLead%2F'+l.id+'%2Fview');
            // reRend.setRedirect(true);
              
            // PageReference testRend = new PageReference('https://nystatesolar--build.lightning.force.com/lightning/r/Lead/'+l.Id+'/view');
            // testRend.setRedirect(true);
              
            // PageReference mobRend = new PageReference('salesforce1://sObject/Lead/'+l.Id+'/view');
            // mobRend.setRedirect(true);  
              // salesforce1://sObject/Opportunity/home
              // 
             // This redirect works for both mobile and desktop 100%  of the time
            String sServerName = ApexPages.currentPage().getHeaders().get('Host');
            sServerName = 'https://' + sServerName + '/';

            System.debug('REDIRECTING TO:'+sServerName+this.l.Id);
            PageReference nmobRend =  new pagereference(sServerName+this.l.Id);
  
            //PageReference nmobRend = new PageReference('https://nystatesolar.my.salesforce.com/'+l.Id);  // this one works 100%
            nmobRend.setRedirect(true);
              
              
            // PageReference leadRend = new PageReference('https://nystatesolar--build.my.salesforce.com/00Q/o');  // this one takes to lead tab hopefully
            // leadRend.setRedirect(true);  
              
              
            //PageReference betaRend = new PageReference('https://nystatesolar--build.my.salesforce.com/lightning/action/quick/Global.FSL__Book_Appointment?objectApiName&recordId='+l.id+'&backgroundContext=%2Flightning%2Fr%2FLead%2F'+l.id+'%2Fview');
            //betaRend.setRedirect(true);  
            // https://nystatesolar--build.my.salesforce.com/00Q29000007tLbx
            
            // This example was a default redirect example used on SFDC  
            // PageReference geRend = new PageReference('https://google.com/');
            // geRend.setRedirect(true);   
           
          
            
            // return reRend;
            //return leadRend;
            //The next uncommented line returns url for mobile and desktop redirect after Create Lead
              return nmobRend;
             
             // 
              /*
             QuickAction.QuickActionRequest req = new QuickAction.QuickActionRequest();
                req.quickActionName = 'Book_Appointment';
                //req.record = c;
                QuickAction.QuickActionResult res = QuickAction.performQuickAction(req);
                //return c.id; 
                
              //  */    
              


         }
            catch(Exception e)
        { Apexpages.addMessage( new ApexPages.Message (ApexPages.Severity.ERROR, 'error is equal to '+e));
       
       return null;
       // https://nystatesolar--build.lightning.force.com/lightning/action/quick/Global.FSL__Book_Appointment?backgroundContext=%2Flightning%2Fr%2FLead%2Fnull%2Fview&recordId=null      
       // https://nystatesolar--build.lightning.force.com/lightning/action/quick/Global.FSL__Book_Appointment?objectApiName&recordId=00Q29000007tmqlEAA&backgroundContext=%2Flightning%2Fr%2FLead%2F00Q29000007tmqlEAA%2Fview
        }
     }
}