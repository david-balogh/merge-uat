@isTest
public class TestPmLeadController {
	  static testMethod void testFollowup() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.LeadPagePM;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        pmLeadController myContr = new pmLeadController(sc);
    	myContr.followup();
    	Test.stopTest();
   }
     static testMethod void testcancelled() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.LeadPagePM;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        pmLeadController myContr = new pmLeadController(sc);
    	myContr.cancelled();
    	Test.stopTest();
   }
  static testMethod void testgoToLead() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.LeadPagePM;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        pmLeadController myContr = new pmLeadController(sc);
    	myContr.goToLead();
    	Test.stopTest();
   }
  static testMethod void testdidntSchedule() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.LeadPagePM;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        pmLeadController myContr = new pmLeadController(sc);
    	myContr.didntSchedule();
    	Test.stopTest();
   }
  static testMethod void testdisqualify() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.LeadPagePM;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        pmLeadController myContr = new pmLeadController(sc);
    	myContr.disqualify();
    	Test.stopTest();
   }
  static testMethod void testnotHome() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.LeadPagePM;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        pmLeadController myContr = new pmLeadController(sc);
    	myContr.notHome();
    	Test.stopTest();
   }
     static testMethod void testreschedulePM() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.LeadPagePM;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        pmLeadController myContr = new pmLeadController(sc);
    	myContr.reschedulePM();
    	Test.stopTest();
   }
   static testMethod void testrescheduleOutreach() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.LeadPagePM;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        pmLeadController myContr = new pmLeadController(sc);
    	myContr.rescheduleOutreach();
    	Test.stopTest();
   }
   static testMethod void testcancel1() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.LeadPagePM;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        pmLeadController myContr = new pmLeadController(sc);
    	myContr.cancel1();
    	Test.stopTest();
   }
   static testMethod void testcancel() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.LeadPagePM;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        pmLeadController myContr = new pmLeadController(sc);
    	myContr.cancel();
    	Test.stopTest();
   }
       static testMethod void testnoInterest() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.LeadPagePM;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        pmLeadController myContr = new pmLeadController(sc);
    	myContr.noInterest();
    	Test.stopTest();
   }
       static testMethod void testsaveDisqualifiedReason() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.LeadPagePM;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        pmLeadController myContr = new pmLeadController(sc);
    	myContr.saveDisqualifiedReason();
    	Test.stopTest();
   }
       static testMethod void testsaveReason() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.LeadPagePM;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        pmLeadController myContr = new pmLeadController(sc);
    	myContr.saveReason();
    	Test.stopTest();
   }
       static testMethod void testdisqualified() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.LeadPagePM;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        pmLeadController myContr = new pmLeadController(sc);
    	myContr.disqualified();
    	Test.stopTest();
   }
       static testMethod void testsave() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.LeadPagePM;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        pmLeadController myContr = new pmLeadController(sc);
    	myContr.save();
    	Test.stopTest();
   }
       static testMethod void testedit() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.LeadPagePM;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        pmLeadController myContr = new pmLeadController(sc);
    	myContr.edit();
    	Test.stopTest();
   }
       static testMethod void testpostOnChatter() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.LeadPagePM;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        pmLeadController myContr = new pmLeadController(sc);
        myContr.detail = 'Hello';
    	myContr.postOnChatter();
    	Test.stopTest();
   }
       static testMethod void testdoPostMessage() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.LeadPagePM;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        pmLeadController myContr = new pmLeadController(sc);
    	myContr.doPostMessage('hello again');
    	Test.stopTest();
   }
   
    
}