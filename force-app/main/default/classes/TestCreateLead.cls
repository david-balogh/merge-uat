@isTest
public class TestCreateLead {
	 static testMethod void testSave() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.mobile_newlead_b_aris;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	//insert leadToCreate;

        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', leadToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        createlead myContr = new createlead(sc);
    	myContr.save();
    	Test.stopTest();
   }
}