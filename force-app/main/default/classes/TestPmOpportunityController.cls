@isTest
public class TestPmOpportunityController {
    
   static testMethod void testNotInterested() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.OpportunityPMPage;
       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Opportunity__c = oppToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Opportunity__c = oppToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(oppToCreate);
        pmOpportunityController myContr = new pmOpportunityController(sc);
    	myContr.notInterested();
    	Test.stopTest();
   }
    
   static testMethod void testNotHome() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.OpportunityPMPage;
       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Opportunity__c = oppToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Opportunity__c = oppToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(oppToCreate);
        pmOpportunityController myContr = new pmOpportunityController(sc);
    	myContr.notHome();
    	Test.stopTest();
   }
    
   static testMethod void testcancelled() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.OpportunityPMPage;
       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Opportunity__c = oppToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Opportunity__c = oppToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(oppToCreate);
        pmOpportunityController myContr = new pmOpportunityController(sc);
    	myContr.cancelled();
    	Test.stopTest();
   }
   
   static testMethod void testfollowup() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.OpportunityPMPage;
       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Opportunity__c = oppToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Opportunity__c = oppToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(oppToCreate);
        pmOpportunityController myContr = new pmOpportunityController(sc);
    	myContr.followup();
    	Test.stopTest();
   }
   static testMethod void testcancelFinal() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.OpportunityPMPage;
       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Opportunity__c = oppToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Opportunity__c = oppToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(oppToCreate);
        pmOpportunityController myContr = new pmOpportunityController(sc);
    	myContr.cancelFinal();
    	Test.stopTest();
   }
   static testMethod void testcancelProposal() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.OpportunityPMPage;
       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Opportunity__c = oppToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Opportunity__c = oppToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(oppToCreate);
        pmOpportunityController myContr = new pmOpportunityController(sc);
    	myContr.cancelProposal();
    	Test.stopTest();
   }
   static testMethod void testeditFinal() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.OpportunityPMPage;
       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Opportunity__c = oppToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Opportunity__c = oppToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(oppToCreate);
        pmOpportunityController myContr = new pmOpportunityController(sc);
        myContr.editFinal();
    	Test.stopTest();
   }
   static testMethod void testeditProposal() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.OpportunityPMPage;
       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Opportunity__c = oppToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Opportunity__c = oppToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(oppToCreate);
        pmOpportunityController myContr = new pmOpportunityController(sc);
        myContr.editProposal();
    	Test.stopTest();
   }
   static testMethod void testsaveFinal() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.OpportunityPMPage;
       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Opportunity__c = oppToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Opportunity__c = oppToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(oppToCreate);
        pmOpportunityController myContr = new pmOpportunityController(sc);
    	myContr.saveFinal();
    	Test.stopTest();
   }
   static testMethod void testsaveProposal() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.OpportunityPMPage;
       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Opportunity__c = oppToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Opportunity__c = oppToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(oppToCreate);
        pmOpportunityController myContr = new pmOpportunityController(sc);
    	myContr.saveProposal();
    	Test.stopTest();
   }
   static testMethod void testpost() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.OpportunityPMPage;
       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Opportunity__c = oppToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Opportunity__c = oppToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(oppToCreate);
        pmOpportunityController myContr = new pmOpportunityController(sc);
        myContr.detail = 'smth';
    	myContr.doPost();
    	Test.stopTest();
   }
}