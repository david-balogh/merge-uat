@istest (SeeAllData=true)
public class TestConvertOnAssignedResourceTrigger {
		static testMethod void tesConvertOnAssignedResource() {
    	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
        // create lead 
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
        // creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
            //Proposal Appointment
        wo.WorkTypeId = '08q4x000000cg3KAAQ';
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo; 
        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        saToCreate.ParentRecordId = wo.Id;
        
		insert saToCreate;
            
        AssignedResource resToInsert = new AssignedResource();
        resToInsert.ServiceAppointmentId = saToCreate.Id;
        //resToInsert.EstimatedTravelTime = 0;
        //resToInsert.FSL__calculated_duration__c = 90;
        resToInsert.ServiceResourceId = '0Hn4x000000h12QCAQ';
        Test.startTest();
        insert resToInsert;
    	Test.stopTest();
   }
}