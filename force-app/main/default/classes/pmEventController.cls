public without sharing class pmEventController {
    public Event myEvent {get; set;}
    public Lead myLead {get; set;}
    public Opportunity myOpportunity {get; set;}
    public Boolean openDetail {get; set;}
    public Boolean showContactMobile {get; set;}
    public Boolean showLeadMobile {get; set;}
    public Boolean hasOpp {get; set;}
    public Boolean hasLead {get; set;}

    public Id userId {get; set;}
    
    public pmEventController(ApexPages.StandardController controller)
    {
        myEvent = (Event) controller.getRecord();
        userId = UserInfo.getUserId();
        myEvent = [SELECT Id, WhoId, WhatId, Subject, Location, Who.type,
                   		Opportunity__c, Lead__c, Service_Appointment__c,
                        ActivityDateTime, Mobile__c, OwnerId
                  		FROM Event
                        WHERE Id = :myEvent.Id LIMIT 1];
        this.openDetail = true;
        if (myEvent.Opportunity__c != null) {
            myOpportunity = [SELECT Id, Name
                     	FROM Opportunity
                     	WHERE Id=:myEvent.Opportunity__c LIMIT 1];
            showContactMobile = true;
            hasOpp = true;
        } else if (myEvent.Lead__c != null) {
            myLead = [SELECT Id, Name, Status,
                      	Not_Interested__c, Not_Qualified_Reason__c
                     	FROM Lead
                     	WHERE Id=:myEvent.Lead__c LIMIT 1];
            showLeadMobile = true;
            hasLead = true;
        }
        
    }
    
    public PageReference goToLead() {
        String sServerName = ApexPages.currentPage().getHeaders().get('Host');
        sServerName = 'https://' + sServerName + '/';

        System.debug('REDIRECTING TO:'+sServerName+myEvent.Lead__c);
        PageReference newPage =  new pagereference(sServerName+myEvent.Lead__c);
               
         newPage.setRedirect(true);
         return newPage ;
    }
    
    public PageReference goToOpportunity() {
		String sServerName = ApexPages.currentPage().getHeaders().get('Host');
        sServerName = 'https://' + sServerName + '/';

        System.debug('REDIRECTING TO:'+sServerName+myEvent.Opportunity__c);
        PageReference newPage =  new pagereference(sServerName+myEvent.Opportunity__c);
               
         newPage.setRedirect(true);
         return newPage ;
    }
}