public without sharing class pmOpportunityController {
	public Opportunity myOpportunity {get; set;}
    public Task myTask {get; set;}
    public Boolean openForFinal {get; set;}
    public Boolean openForProposal {get; set;}
    public Boolean openDisqualified {get; set;}
    public String detail { get; set;}
    public String message { get; set; }
    public Boolean editForFinal {get; set;}
    public Boolean editForProposal {get; set;}
    public List<FeedItem> evItems {get; set;}
    public Boolean hasChatter {get; set;}
    public Id userId {get; set;}
    public Boolean hasSA {get; set;}
    public List<ServiceAppointment> sapps {get; set;}
    public String PMStatus {get; set;}
    public String addr {get; set;}
    
    public pmOpportunityController(ApexPages.StandardController controller) {
        myOpportunity = (Opportunity) controller.getRecord();
        userId = UserInfo.getUserId();
        myOpportunity = [SELECT Id, Install_Address__c, StageName, kW_Used__c, Homeowner_1__c,
						Number_of_Layers__c, Finance_Recommendation__c, Estimated_Interest_Rate__c,
						LeadSource, Utility_Provider__c, Account_Number__c, Number_of_Family_Members__c,
						Household_Income__c, Design_Aurora_Production__c, AccountId, Name, Homeowner_2__c,
						Design_Panel_Count__c, Design_System_Size__c, Estimated_1_Year_Production_kWh__c,
						Estimated_Annual_Electric_Usage_kWh__c, Average_TSRF__c, Estimated_Bill_Credits__c,
						Offset__c, Roof_replacement_additional_quote__c, Upgrade_Note__c, Primary_Loan_Amount1__c,
						Secondary_Loan_Amount1__c, Total_Contract_Amount1__c, RecordType.Name, Mobile__c, Homeowner_1_Email__c 
                  		FROM Opportunity
                        WHERE Id = :myOpportunity.Id LIMIT 1];
        addr =  myOpportunity.Install_Address__c;
        addr = addr.replaceAll('<br>', ', ');
        sapps = [SELECT Id, Status, Opportunity__c FROM ServiceAppointment WHERE Opportunity__c =: myOpportunity.Id AND Status IN ('None','Dispatched','In Progress', 'Scheduled')];
        if (sapps.size() > 0) {
            this.hasSA = true;
        }
        // get this value from a Custom Setting
        if(myOpportunity.StageName == 'Design' || myOpportunity.StageName == 'Adopter' ) {
            this.openForProposal = true;
        } else {
            this.openForFinal = true;
        }
        evItems = [SELECT Id, body, CreatedBy.FirstName, CreatedBy.LastName,
                       CreatedDate, ParentId, Parent.Name, Type
                       FROM FeedItem 
                       WHERE ParentId=:myOpportunity.Id
                       AND Type = 'TextPost'
                       ORDER BY CreatedDate DESC LIMIT 5];
            //AdvancedTextPost
        if(evItems.size() > 0) {
                hasChatter = true;
        }
        // Setting up PM Status
        PMStatus = 'Not Set';
        if (myOpportunity.StageName == 'Design') {
            PMStatus = 'Proposal Appointment';
        } else if (myOpportunity.RecordType.Name == 'Contract Opportunity') {
            	PMStatus = 'Final Appointment';
        }
        if (myOpportunity.StageName == 'Closed Won' || myOpportunity.StageName == 'Permission to Operate (PTO)' || myOpportunity.StageName == 'Closed Won' ) {
            	PMStatus = 'Installed';
        } else if (myOpportunity.RecordType.Name == 'Install Opportunity') {
                PMStatus = 'In Progress';  
         }
    }
    
    public PageReference doPost() {
        FeedItem fItem = new FeedItem();

        //show up in the Lead's feed then change to set parent to myOpportunity Id

        fItem.parentId = this.myOpportunity.Id;
        if(detail != null && detail != '') {
            fItem.body = detail;
        	insert fItem;
        	detail = '';
           	String sServerName = ApexPages.currentPage().getHeaders().get('Host');
            sServerName = 'https://' + sServerName + '/';

            System.debug('REDIRECTING TO:'+sServerName+this.myOpportunity.Id);
            PageReference newPage =  new pagereference(sServerName+this.myOpportunity.Id);
              
            newPage.setRedirect(true);
        	// If I apply this, I won't be able to see the change on chatter(it doesen't refresh)
        	// it only refreshes the view
        	/*ApexPages.StandardController sc = new ApexPages.StandardController(myOpportunity);

    		PageReference newPage = sc.view();

    		newPage.setRedirect(true);*/

            return newPage;
        }
        return null;
    }
    
    public void editProposal() {
        this.editForProposal = true;
        this.openForProposal = false;
    }
    
    public void editFinal() {
        this.openForFinal = false;
        this.editForFinal = true;
    }
    
    public void saveProposal() {
        this.cancelProposal();
        //more logic to come for save
        //onsave update the changed record
        update this.myOpportunity;
    }
    
    public void saveFinal() {
        this.cancelFinal();
        //more logic to come for save
        //onsave update the changed record
        update this.myOpportunity;
    }
    
    public void cancelProposal() {
        this.editForProposal = false;
        this.openForProposal = true;
    }
    
    public void cancelFinal() {
        this.openForFinal = true;
        this.editForFinal = false;
    }
    
    
    public void notHome() {
        for (ServiceAppointment sa : sapps) {
            sa.Status = 'Cannot Complete';
        }
        try {
            update sapps;
        } catch (DMLException e) {
            System.debug('not home Failed ' + e.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'SA update failed' + e.getMessage());
        }
    }
    
    public void cancelled() {
        for (ServiceAppointment sa : sapps) {
            sa.Status = 'Canceled';
        }
        try {
            update sapps;
        } catch (DMLException e) {
            System.debug('cancelled Failed ' + e.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'SA update failed' + e.getMessage());
        }
    }
    public void followup() {
        ServiceAppointment sapp = [SELECT Id, Status, Opportunity__c, CreatedDate FROM ServiceAppointment WHERE Opportunity__c =: myOpportunity.Id AND Status IN ('None','Dispatched','In Progress', 'Scheduled') ORDER BY CreatedDate DESC LIMIT 1];
        sapp.status = 'Completed';
        try {
            update sapp;
            update myOpportunity;
        } catch (DMLException e) {
            System.debug('cancelled Failed ' + e.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'SA/opp update failed during follow up action' + e.getMessage());
        }
    }
    
    public PageReference notInterested() {
        this.myOpportunity.StageName = 'Closed/ Contract Lost';
        this.myOpportunity.Loss_Reason__c = 'Other';
        ServiceAppointment sapp = [SELECT Id, Status, Opportunity__c, CreatedDate FROM ServiceAppointment WHERE Opportunity__c =: myOpportunity.Id AND Status IN ('None','Dispatched','In Progress', 'Scheduled') ORDER BY CreatedDate DESC LIMIT 1];
		sapp.status = 'Completed';
        try {
            update sapp;
            update myOpportunity;
            String sServerName = ApexPages.currentPage().getHeaders().get('Host');
        	sServerName = 'https://' + sServerName + '/';
	
        	System.debug('REDIRECTING TO:'+sServerName+this.myOpportunity.Id);
        	PageReference newPage =  new pagereference(sServerName+this.myOpportunity.Id);
        	      
        	newPage.setRedirect(true);
        	return newPage;
        } catch (DMLException e) {
            System.debug('cancelled Failed ' + e.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'SA/opp update failed during notInterested action' + e.getMessage());
        }
        return null;
    }
}