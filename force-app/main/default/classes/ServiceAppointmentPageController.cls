public without sharing class ServiceAppointmentPageController {

    public ServiceAppointment myServiceAppointment {get; set;}
    public Boolean hasOpp {get; set;}
    public Boolean hasLead {get; set;}

    public Id userId {get; set;}
    
    public ServiceAppointmentPageController(ApexPages.StandardController controller)
    {
        myServiceAppointment = (ServiceAppointment) controller.getRecord();
        userId = UserInfo.getUserId();
        myServiceAppointment = [SELECT Id,
                   		Opportunity__c, Lead__c
                  		FROM ServiceAppointment
                        WHERE Id = :myServiceAppointment.Id LIMIT 1];
        if (myServiceAppointment.Opportunity__c != null) {
            hasOpp = true;
        } else if (myServiceAppointment.Lead__c != null) {
            hasLead = true;
        }
    }
    
    public PageReference goToLead() {
        String sServerName = ApexPages.currentPage().getHeaders().get('Host');
        sServerName = 'https://' + sServerName + '/';

        System.debug('REDIRECTING TO:'+sServerName+myServiceAppointment.Lead__c);
        PageReference newPage =  new pagereference(sServerName+myServiceAppointment.Lead__c);
               
         newPage.setRedirect(true);
         return newPage ;
    }
    
    public PageReference goToOpportunity() {
		String sServerName = ApexPages.currentPage().getHeaders().get('Host');
        sServerName = 'https://' + sServerName + '/';

        System.debug('REDIRECTING TO:'+sServerName+myServiceAppointment.Opportunity__c);
        PageReference newPage =  new pagereference(sServerName+myServiceAppointment.Opportunity__c);
               
         newPage.setRedirect(true);
         return newPage ;
    }
}