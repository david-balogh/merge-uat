public without sharing class pmLeadController {
    public Lead myLead {get; set;}
    public Id userId {get; set;}
    public Boolean openEdit {get; set;}
    public Boolean openDetail {get; set;}
    public Boolean openDisqualified {get; set;}
    public Boolean openNotInterested {get; set;}
    public Boolean showEditButton {get; set;}
    public Boolean convertError {get; set;}
    public Boolean hasChatter {get; set;}
    public Boolean hasSA {get; set;}
    public Boolean convertedLead {get; set;}
    public String detail1 {get; set;}
    public String PMStatus {get; set;}
    public String detail { get; set;}
    public List<FeedItem> evItems {get; set;}
    public List<ServiceAppointment> sapps {get; set;}
    
    public pmLeadController(ApexPages.StandardController controller) 
    {
        this.openDetail = true;
        myLead = (Lead) controller.getRecord();
        userId = UserInfo.getUserId();
        myLead = [SELECT Id, Name, FirstName, LastName, Email, Number_of_Family_Members__c, Status,
                  Household_Income__c, kW_Used__c, Finance_Recommendation__c, Not_Interested__c,
                  Not_Qualified_Reason__c, Start_Conversion__c, isConverted, Payable_Date__c,
                  Street, Homeowner_2_Email__c, Homeowner_2_First_Name__c, Country, MobilePhone,
                  Alternate_Email__c, Homeowner_2_Alternate_Email__c, City, State, PostalCode, Payable_lead1__c,
                  Homeowner_2_Last_Name__c, Homeowner_2_Phone__c, Disqualified_Reason__c, Disposition__c,
                  ConvertedOpportunityId
                  from Lead where Id = :mylead.Id LIMIT 1];
        PMStatus = 'Intro Appointment';
        if(this.myLead.Status != 'Not Interested' && this.myLead.Status != 'Not Qualified'){
            this.showEditButton = true;
            if (this.myLead.Status == 'Rescheduled PM') {
                PMStatus = 'Reschedule PM';
            }
            if (this.myLead.Status == 'Rescheduled not PM') {
                PMStatus = 'For Follow-up';
            }
        } else {
            this.showEditButton = false;
            if (this.myLead.Status == 'Not Interested') {
                PMStatus = 'Not Interested';
            }
            if (this.myLead.Status == 'Not Qualified') {
                PMStatus = 'Not Qualified';
            }
        }
        sapps = [SELECT Id, Status, Lead__c FROM ServiceAppointment WHERE Lead__c =: myLead.Id AND Status IN ('None','Dispatched','In Progress', 'Scheduled')];
        if (sapps.size() > 0) {
            this.hasSA = true;
        }
        evItems = [SELECT Id, body, CreatedBy.FirstName, CreatedBy.LastName,
                       CreatedDate, ParentId, Parent.Name, Type
                       FROM FeedItem 
                       WHERE ParentId=:myLead.Id
                       AND Type = 'TextPost'
                       ORDER BY CreatedDate DESC LIMIT 5];
            //AdvancedTextPost
        if(evItems.size() > 0) {
        	hasChatter = true;
        }
        if (this.myLead.IsConverted) {
            this.convertedLead = true;
            this.showEditButton = false;
            this.openDetail = false;
            this.openEdit = false;
        }
       	
    }
        
    public PageReference doPostMessage(String message) {
        FeedItem fItem = new FeedItem();
        fItem.parentId = this.myLead.Id;
        fItem.body = message;
        insert fItem;
        return null;
    }
    
    public PageReference postOnChatter() {
        if(detail != null && detail != '') {
        	FeedItem fItem = new FeedItem();
        	fItem.parentId = this.myLead.Id;
        	fItem.body = detail;
        	insert fItem;
            detail = '';
        	return goToLead();
        }
        return null;
    }
    
    public void edit() {
        this.openEdit = true;
        this.openDetail = false;
    }
    
    public PageReference save() {
        this.cancel();
        //onsave update the changed record
        update this.myLead;
        return goToLead();
    }    
    
    public void disqualified() {
        this.myLead.Status = 'Not Qualified';
        this.openDisqualified = true;
        this.cancel();
        this.openDetail = false;
    }
    
    public PageReference saveReason() {
        this.openDisqualified = false;
        this.cancel();
        this.openDetail = false;
        if (this.myLead.Not_Qualified_Reason__c == 'Other') {
            this.openNotInterested = true;
            return null;
        } else {
           this.save();
           return this.cancel1();
        }
        
    }
    
    public void noInterest() {
		this.myLead.Status = 'Not Interested';
        this.openNotInterested = true;
        this.cancel();
        this.openDetail = false;
    }
    
    public PageReference saveDisqualifiedReason() {
        this.openNotInterested = false;
        this.save();
        update this.myLead;
        String msg = 'Disqualified Reason: ' + this.myLead.Not_Interested__c;
        this.doPostMessage(msg);
        return this.cancel1();
    }
    
    public void cancel() {
        this.openEdit = false;
        this.openDetail = true;
    }
    
    public PageReference cancel1() {
        String sServerName = ApexPages.currentPage().getHeaders().get('Host');
        sServerName = 'https://' + sServerName + '/';

        System.debug('REDIRECTING TO:'+sServerName+this.myLead.Id);
        PageReference newPage =  new pagereference(sServerName+this.myLead.Id);
               
        newPage.setRedirect(true);
        return newPage ;
    }
    
    public PageReference rescheduleOutreach() {
        myLead.Status = 'Rescheduled not PM';
        this.save();
        
        return this.cancel1();
    }
    
    public PageReference reschedulePM() {
        myLead.Status = 'Rescheduled PM';
        this.save();
        
        return this.cancel1();
    }
    
    public void notHome() {
        for (ServiceAppointment sa : sapps) {
            sa.Status = 'Cannot Complete';
        }
        try {
            update sapps;
        } catch (DMLException e) {
            System.debug('Lead Convert Failed ' + e.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'SA update failed' + e.getMessage());
        }
    }
    
    public PageReference disqualify() {
        for (ServiceAppointment sa : sapps) {
            sa.Status = 'Completed';
        }
        myLead.Status = 'Not Qualified';
        myLead.Not_Qualified_Reason__c = 'Poor site quality';
        try {
           update sapps;
           update myLead;
        } catch (DMLException e) {
            System.debug('Lead Convert Failed ' + e.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'SA/Lead updates failed' + e.getMessage());
        }

        return goToLead();
    }
    
    public PageReference didntSchedule() {
        for (ServiceAppointment sa : sapps) {
            sa.Status = 'Cannot Complete';
        }
        myLead.Status = 'Not Interested';
        myLead.Not_Interested__c = 'Poor site quality';
        try {
           update sapps;
           update myLead;
        } catch (DMLException e) {
            System.debug('Lead Convert Failed ' + e.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'SA/Lead updates failed' + e.getMessage());
        }

        return goToLead();
    }
    
    public PageReference goToLead() {
        String sServerName = ApexPages.currentPage().getHeaders().get('Host');
        sServerName = 'https://' + sServerName + '/';
        PageReference newPage =  new pagereference(sServerName+myLead.Id);
               
         newPage.setRedirect(true);
         return newPage ;
    }
    
    public PageReference goToOpp() {
        String sServerName = ApexPages.currentPage().getHeaders().get('Host');
        sServerName = 'https://' + sServerName + '/';
        PageReference newPage =  new pagereference(sServerName+myLead.ConvertedOpportunityId);
               
         newPage.setRedirect(true);
         return newPage ;
    }
    
    public void cancelled() {
        for (ServiceAppointment sa : sapps) {
            sa.Status = 'Canceled';
        }
        try {
            update sapps;
        } catch (DMLException e) {
            System.debug('Lead Convert Failed ' + e.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'SA update failed' + e.getMessage());
        }
    }
    
     public void followup() {
        ServiceAppointment sapp = [SELECT Id, Status, Lead__c, CreatedDate FROM ServiceAppointment WHERE Lead__c =: myLead.Id AND Status IN ('None','Dispatched','In Progress', 'Scheduled') ORDER BY CreatedDate DESC LIMIT 1];
        sapp.status = 'Completed';
        myLead.Payable_Date__c = Date.today();
        try {
            update sapp;
            update myLead;
        } catch (DMLException e) {
            System.debug('cancelled Failed ' + e.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'SA/opp update failed during follow up action' + e.getMessage());
        }
    }
}