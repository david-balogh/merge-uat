@isTest
public class TestCountProjectTasksTrigger {
	static testMethod void testCheckNotCompleted() {
    	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        
        //create project and link
      	Project__c prj = new Project__c();
        prj.Opportunity__c = oppToCreate.Id;
        insert prj;
        
        // create tasks open and closed;
        Task tsk = new Task();
        tsk.WhatId = prj.Id;
        tsk.Subject = 'Call';
        tsk.Opportunity__c = oppToCreate.Id;
        tsk.Status = 'Open';
        insert tsk;
        
               
        // Perform test
        Test.startTest();
        Database.SaveResult result = Database.update(prj, false);
        System.assert(true, result.isSuccess());
        System.assert(prj.Status__c != 'Completed');
        Test.stopTest();
        // I have to Add tasks to the project and also ad a project to the 
        // 
        // /create task where whatid is the project
   }
   
   static testMethod void testCheckCompleted() {
    	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        
        //create project and link
      	Project__c prj = new Project__c();
        prj.Opportunity__c = oppToCreate.Id;
        insert prj;
        
        // create tasks open and closed;
        Task tsk = new Task();
        tsk.WhatId = prj.Id;
        tsk.Subject = 'Call';
        tsk.Opportunity__c = oppToCreate.Id;
        tsk.Status = 'Completed';
        insert tsk;
        
               
        // Perform test
        Test.startTest();
        Database.SaveResult result = Database.update(prj, false);
        System.assert(true, result.isSuccess());
        System.assert(prj.Status__c != 'Completed');
        Test.stopTest();
        // I have to Add tasks to the project and also ad a project to the 
        // 
        // /create task where whatid is the project
   }
}