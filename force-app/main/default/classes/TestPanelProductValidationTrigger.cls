@isTest
public class TestPanelProductValidationTrigger {
	   static testMethod void testInsertOppLineItem() {
    	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
        // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
           
        
        //product2
        Product2 prd = new Product2();
        prd.Name = 'Panel';
        RecordType rt = [SELECT ID from RecordType where Name IN ('Panels') Limit 1];
        prd.RecordTypeId = rt.Id;
        prd.IsActive = true;
        prd.Watts__c = 23;
        insert prd;
        
        //create pricebook2
        //Pricebook2 pbk2 = new Pricebook2();
        //pbk2.Name = 'PBK';
        //pbk2.IsActive = true;
        //insert pbk2;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        //pricebookentry
        PricebookEntry pbentry = new PricebookEntry();
        pbentry.UnitPrice = 12;
        pbentry.Product2Id = prd.Id;
        pbentry.Pricebook2Id = pricebookId;
        pbentry.UnitPrice = 123;
        pbentry.UseStandardPrice = false;
        pbentry.IsActive = true;
        insert pbentry;
           
        OpportunityLineItem oppItem = new OpportunityLineItem();
        oppItem.OpportunityId = oppToCreate.Id;
        oppItem.Quantity = 2;
        oppItem.Product2Id = prd.Id;
        oppItem.TotalPrice = 12345;
        oppItem.PricebookEntryId = pbEntry.Id;
		     
		 // Perform test
        Test.startTest();
        insert oppItem;   
      	System.assertEquals(oppItem.System_Size__c, oppToCreate.Opportunity_System_Size__c);
        Test.stopTest();
   }
}