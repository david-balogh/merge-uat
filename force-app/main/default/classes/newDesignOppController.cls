public class newDesignOppController {
    public Opportunity myOpportunity {get; set;}
    public Design__c myNewDesign {get; set;}
    public OpportunityContactRole myContactRole {get; set;}
    public Boolean showNewAurora {get; set;}
    public Boolean showNewCad {get; set;}
    public Boolean showNewPlanSet {get; set;}
    public Boolean showButtons {get; set;}
    
    public newDesignOppController(ApexPages.StandardController controller) {
        myOpportunity = (Opportunity) controller.getRecord();
        myOpportunity = [SELECT Id, Install_Address__c, StageName, kW_Used__c, Homeowner_1__c,
                        Number_of_Layers__c, Finance_Recommendation__c, Estimated_Interest_Rate__c,
                        LeadSource, Utility_Provider__c, Account_Number__c, Number_of_Family_Members__c,
                        Household_Income__c, Design_Aurora_Production__c, AccountId, Name,
                        Design_Panel_Count__c, Design_System_Size__c, Estimated_1_Year_Production_kWh__c,
                        Estimated_Annual_Electric_Usage_kWh__c, Average_TSRF__c, Estimated_Bill_Credits__c,
                        Offset__c, Roof_replacement_additional_quote__c, Upgrade_Note__c, Primary_Loan_Amount1__c,
                        Secondary_Loan_Amount1__c, Total_Contract_Amount1__c
                        FROM Opportunity
                        WHERE Id = :myOpportunity.Id LIMIT 1];
        myNewDesign = new Design__c();
        this.showButtons = true;
    }
    
    public PageReference save() {
        try {
            myNewDesign.Id = null;
            insert myNewDesign;
            return this.cancel();
        } catch (DmlException e) {
            System.debug('New Design prefilled Failed: ' + e.getMessage());
        }
        return null;
    }
    
    public void newAurora() {
        this.showNewAurora = true;
        this.showButtons = false;
        List<Design__c> auxDesign = new List<Design__c>();
        auxDesign = [SELECT Id, Status__c, Panel_Count__c, Panel_Type__c, System_Cost__c, Number_of_Steep_Roofs__c,
                            Number_of_Flat_Roofs__c, Number_of_Multi_Array_Panels__c, Aurora_Production__c,
                            OOP_to_OBR__c, OOP_to_SEL__c, Opportunity__c, Product_Trench__c, Electrical_Work__c,
                            Roof_Quote__c, Trenching_Included__c, Product__c, Soft_Roof_Quote__c, Trenching_Price__c,
                            CreatedDate, RecordTypeId
                       FROM Design__c
                       WHERE Opportunity__c =: this.myOpportunity.Id and RecordType.Name='Aurora'
                       ORDER BY CreatedDate DESC LIMIT 1];
        
        if (auxDesign.size() > 0) {
            myNewDesign = auxDesign[0];
        } else {
            auxDesign = [SELECT Id, Status__c, Panel_Count__c, Panel_Type__c, System_Cost__c, Number_of_Steep_Roofs__c,
                            Number_of_Flat_Roofs__c, Number_of_Multi_Array_Panels__c, Aurora_Production__c,
                            OOP_to_OBR__c, OOP_to_SEL__c, Opportunity__c, Product_Trench__c, Electrical_Work__c,
                            Roof_Quote__c, Trenching_Included__c, Product__c, Soft_Roof_Quote__c, Trenching_Price__c,
                            CreatedDate, RecordTypeId
                       FROM Design__c
                       WHERE Opportunity__c =: this.myOpportunity.Id
                       ORDER BY CreatedDate DESC LIMIT 1];
            if (auxDesign.size() > 0) {
                myNewDesign = auxDesign[0];
                myNewDesign.RecordTypeId = '0124x000000Mb31AAC';
            }
        }
        
    }
    
    public void newCad() {
        this.showNewCad = true;
        this.showButtons = false;
        List<Design__c> auxDesign = new List<Design__c>();
        auxDesign = [SELECT Id, Status__c, Panel_Count__c, Panel_Type__c, System_Cost__c, Number_of_Steep_Roofs__c,
                        Number_of_Flat_Roofs__c, Number_of_Multi_Array_Panels__c, Aurora_Production__c,
                        Share_Report_Uploaded__c, Rooft_Rating__c, Opportunity__c, Product_Trench__c, Site_Capture_Link__c,
                        Product__c, Soft_Roof_Quote__c, CreatedDate, RecordTypeId
                       FROM Design__c
                       WHERE Opportunity__c =: this.myOpportunity.Id and RecordType.Name='CAD'
                       ORDER BY CreatedDate DESC LIMIT 1];
        
        if (auxDesign.size() > 0) {
            myNewDesign = auxDesign[0];
        }  else {
            auxDesign = [SELECT Id, Status__c, Panel_Count__c, Panel_Type__c, System_Cost__c, Number_of_Steep_Roofs__c,
                        Number_of_Flat_Roofs__c, Number_of_Multi_Array_Panels__c, Aurora_Production__c,
                        Share_Report_Uploaded__c, Rooft_Rating__c, Opportunity__c, Product_Trench__c, Site_Capture_Link__c,
                        Product__c, Soft_Roof_Quote__c, CreatedDate, RecordTypeId
                       FROM Design__c
                       WHERE Opportunity__c =: this.myOpportunity.Id
                       ORDER BY CreatedDate DESC LIMIT 1];
            if (auxDesign.size() > 0) {
                myNewDesign = auxDesign[0];
                myNewDesign.RecordTypeId = '0124x000000Mb32AAC';
            }
        }
    }
    
    public void newPlanSet() {
        this.showNewPlanSet = true;
        this.showButtons = false;
        List<Design__c> auxDesign = new List<Design__c>();
        auxDesign = [SELECT Id, Status__c, Panel_Count__c, Panel_Type__c, System_Cost__c, Number_of_Steep_Roofs__c,
                            Number_of_Flat_Roofs__c, Number_of_Multi_Array_Panels__c, Aurora_Production__c,
                            Opportunity__c, Product_Trench__c, Interior_Redo_Required__c, Interior_Surveyor__c, 
                            Product__c, Soft_Roof_Quote__c, CreatedDate, RecordTypeId
                       FROM Design__c
                       WHERE Opportunity__c =: this.myOpportunity.Id and RecordType.Name='Plan Set'
                       ORDER BY CreatedDate DESC LIMIT 1];
        
        if (auxDesign.size() > 0) {
            myNewDesign = auxDesign[0];
        } else {
            auxDesign = [SELECT Id, Status__c, Panel_Count__c, Panel_Type__c, System_Cost__c, Number_of_Steep_Roofs__c,
                            Number_of_Flat_Roofs__c, Number_of_Multi_Array_Panels__c, Aurora_Production__c,
                            Opportunity__c, Product_Trench__c, Interior_Redo_Required__c, Interior_Surveyor__c, 
                            Product__c, Soft_Roof_Quote__c, CreatedDate, RecordTypeId
                       FROM Design__c
                       WHERE Opportunity__c =: this.myOpportunity.Id
                       ORDER BY CreatedDate DESC LIMIT 1];
            if (auxDesign.size() > 0) {
                myNewDesign = auxDesign[0];
                myNewDesign.RecordTypeId = '0124x000000Mb33AAC';
            }
        }
    }
    
    public PageReference cancel() {
        
            String sServerName = ApexPages.currentPage().getHeaders().get('Host');
            sServerName = 'https://' + sServerName + '/';

            System.debug('REDIRECTING TO:'+sServerName+this.myOpportunity.Id);
            PageReference newPage =  new pagereference(sServerName+this.myOpportunity.Id);
              
            newPage.setRedirect(true);

            return newPage;
        
    }
        
}