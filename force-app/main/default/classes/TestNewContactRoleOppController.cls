@isTest
public class TestNewContactRoleOppController {
	static testMethod void testCancel() {
    	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.newContactRoleOpportunity;
       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', oppToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(oppToCreate);
        newContactRoleOppController myContr = new newContactRoleOppController(sc);
    	myContr.cancel();
    	Test.stopTest();
   }
    
   	static testMethod void testsave() {
    	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.newContactRoleOpportunity;
       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;

        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', oppToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(oppToCreate);
        newContactRoleOppController myContr = new newContactRoleOppController(sc);
    	myContr.save();
    	Test.stopTest();
   }
}