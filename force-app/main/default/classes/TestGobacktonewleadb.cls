@isTest
public class TestGobacktonewleadb {
	static testMethod void testFollowup() {
       	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.LeadPagePM;
        //create lead
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        gobacktonewleadb myContr = new gobacktonewleadb(sc);
    	myContr.save();
    	Test.stopTest();
   }
}