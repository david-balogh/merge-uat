public without sharing class ContactPageController {
    
	public Contact myContact {get; set;}
    public Boolean hasOpp {get; set;}
    public Id userId {get; set;}
    
    public ContactPageController(ApexPages.StandardController controller)
    {
        myContact = (Contact) controller.getRecord();
        userId = UserInfo.getUserId();
        myContact = [SELECT Id, Opportunity__c
                  		FROM Contact
                        WHERE Id = :myContact.Id LIMIT 1];
        if (myContact.Opportunity__c != null) {
            hasOpp = true;
        }
    }
    
    public PageReference goToOpportunity() {
		/*String sServerName = ApexPages.currentPage().getHeaders().get('Host');
        sServerName = 'https://' + sServerName + '/';

        System.debug('REDIRECTING TO:'+sServerName+myContact.Opportunity__c);
        PageReference newPage =  new pagereference(sServerName+myContact.Opportunity__c);*/
        // this kind of works
		PageReference newPage = Page.OpportunityPMPage;     
        newPage.getParameters().put('Id', mycontact.Opportunity__c);
         newPage.setRedirect(true);
         return newPage ;
    }
   	
}