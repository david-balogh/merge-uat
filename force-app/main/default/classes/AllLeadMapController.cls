public without sharing class AllLeadMapController {
    public List<Lead> allLeads {get; set;}
    public List<Opportunity> allOppts {get; set;}
    public Id userId {get; set;}
    public String tabOpt {get;set;}
    public Boolean isOutreach {get; set;}
    Integer i = 1;
    
    public AllLeadMapController(ApexPages.StandardController controller)
    {
        tabOpt = 'Tab1';
        userId = UserInfo.getUserId();
        Id profileId = UserInfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        if (profileName == 'PM2' || profileName == 'System Administrator') {
           allLeads = [SELECT Id, Name, Address, OwnerId, Street, City, State, PostalCode
                  		FROM Lead
                        WHERE OwnerId = :userId];
        	allOppts = [SELECT Id, Name, OwnerId, Install_Address__c
						FROM Opportunity
                   		WHERE OwnerId = :userId]; 
        }
        if (profileName == 'Outreach') {
           allLeads = [SELECT Id, Name, Address, Lead_Creator__c, Street, City, State, PostalCode
                  		FROM Lead
                        WHERE Lead_Creator__c = :userId];
            isOutreach = true;
        }

	}
    
    public void switch() {
        i++;
       	i = (i > 2) ? 1 : i;
		tabOpt = 'Tab'+i;
	} 
}