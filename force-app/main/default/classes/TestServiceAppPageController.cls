@isTest
public class TestServiceAppPageController {
	static testMethod void testGoToLead() {
    	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.ServiceAppointmentPage;
        // create lead 
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
        // creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Lead__c = leadToCreate.Id;
		insert wo; 
        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Lead__c = leadToCreate.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        saToCreate.ParentRecordId = wo.Id;
        
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(saToCreate);
        ServiceAppointmentPageController myContr = new ServiceAppointmentPageController(sc);
    	myContr.goToLead();
    	Test.stopTest();
   }
    
   static testMethod void testGoToOpp() {
    	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.ServiceAppointmentPage;
       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Opportunity__c = oppToCreate.Id;
		insert wo;        
        //create ServiceAppointment and link
      	ServiceAppointment saToCreate = new ServiceAppointment();
        saToCreate.Subject = 'TEST';
        saToCreate.OwnerId = userToCreate.id;
        saToCreate.Opportunity__c = oppToCreate.Id;
        saToCreate.ParentRecordId = wo.Id;
        saToCreate.SchedStartTime = Datetime.now();
        saToCreate.SchedEndTime = Datetime.now() + 1;
        insert saToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', saToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(saToCreate);
        ServiceAppointmentPageController myContr = new ServiceAppointmentPageController(sc);
    	myContr.goToOpportunity();
    	Test.stopTest();
   }
}