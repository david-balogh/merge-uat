public class AutoConvertLeads {
	@InvocableMethod   
    public static void LeadAssign(List<Id> LeadIds)  
    {         
        LeadStatus CLeadStatus= [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true Limit 1];    
        List<Database.LeadConvert> MassLeadconvert = new List<Database.LeadConvert>();    
        for(id currentlead: LeadIds){                
            Database.LeadConvert Leadconvert = new Database.LeadConvert();    
            Leadconvert.setLeadId(currentlead);                               
            Leadconvert.setConvertedStatus(CLeadStatus.MasterLabel);             
            MassLeadconvert.add(Leadconvert);    
        }                  
        if (!MassLeadconvert.isEmpty())
        {            
            Map<Id, Id> leadsAndOppsMap = new Map<Id, Id>();
            //map for PB Lead is Converted
            Map<Id, Id> oppAndContactMap = new Map<Id, Id>(); 
            List<Database.LeadConvertResult> lcr = Database.convertLead(MassLeadconvert);
            for(Database.LeadConvertResult lc : lcr) {
                if (lc.isSuccess()) {
                    Id oppId = lc.getOpportunityId();
                    Id leadId = lc.getLeadId();
                    leadsAndOppsMap.put(leadId, oppId);
                    
                    //logic for PB Lead is Converted
                    Id contactId = lc.getContactId();
                    oppAndContactMap.put(oppId,contactId);
                }
            }
            // Get All Leads involved
            List<Lead> leadToUpdate = [SELECT Id, FirstName, LastName, LeadSource, Street, PostalCode, Service_Appointment_DateTime__c, Lead_Creator__c,
                                       Homeowner_2_Last_Name__c, Homeowner_2_Alternate_Email__c, Homeowner_2_Email__c, Homeowner_2_First_Name__c, Disposition__c,
                                       Homeowner_2_Phone__c, MobilePhone, Alternate_Email__c, Website_Lead_Source__c, ConvertedContactId, Payable_lead1__c, Email  
                                       FROM Lead WHERE Id IN : leadsAndOppsMap.keySet()];
            List<Id> leadCreators  = new List<Id>();
            for (Lead l : leadToUpdate) {
                leadCreators.add(l.Lead_Creator__c);
            }
            List<OpportunityTeamMember> oppTeamMembers = new List<OpportunityTeamMember>();
            List<Contact> contactsToAdd = new List<Contact>();
            List<Contact> contactsToUpdate = new List<Contact>();
            List<Id> cntIds = new List<Id>();
            Map<Id, User> usersCreatorsById = new Map<Id, User>([SELECT Id, isActive from User where Id IN :leadCreators]);
            //Update Opp for PB Lead is converted 
            List<Opportunity> oppsToUpdate = [SELECT Id, Homeowner_1__c,Lead_Creator__c, LeadSource, Website_Lead_Source__c, Service_Appointment_Date__c, Name,
                                              AccountId, Homeowner_1_Email__c, StageName, Mobile__c
                                              FROM Opportunity WHERE Id IN : oppAndContactMap.keySet()];
            for (Opportunity opp: oppsToUpdate) {
                opp.Homeowner_1__c = oppAndContactMap.get(opp.Id);
                cntIds.add(oppAndContactMap.get(opp.Id));
                for (Lead l : leadToUpdate) {
                	if (opp.Id == leadsAndOppsMap.get(l.Id)) {
                    	opp.LeadSource = l.LeadSource;
                        opp.Name = l.Firstname + ' ' + l.LastName + ', ' + l.Street + ', ' + l.PostalCode + '- Contract';
                        opp.Service_Appointment_Date__c = l.Service_Appointment_DateTime__c;
                        opp.Website_Lead_Source__c = l.Website_Lead_Source__c;
                        // new additon => Homeowner 1 Email created On Opportunty
                        opp.Homeowner_1_Email__c = l.Email;
                        opp.Mobile__c = l.MobilePhone;
                        // these two are happening in a process builder New Assigned resource, We need to set this up for the new PB for the Disposition__c
                        //l.Disposition__c = 'Proposal Appointment - P';
                        //l.Payable_lead1__c = true;
                        //
                        //Checking if the Lead disposition is DQ-Tax Liability or DQ-Permitting Issue
                        if (l.Disposition__c == 'DQ - Permitting Issues' || l.Disposition__c == 'DQ - Tax Liability') {
                            opp.StageName = 'Closed/ DQ Lost';
                        }
                        // Adding the lead creator as an Opportunity Team member
                      
                        User u = usersCreatorsById.get(l.Lead_Creator__c);
                        if (u.IsActive) {
                            // set lead Creator
                            opp.Lead_Creator__c = l.Lead_Creator__c;
                            // Create OpptyTeamMember
                            OpportunityTeamMember otm = new OpportunityTeamMember();
                            otm.OpportunityId = opp.Id;
                            otm.UserId = u.Id;
                            //otm.OpportunityAccessLevel = 'Read Only'; (it was marked as not editable)
                            otm.TeamMemberRole = 'Lead Creator';
                            oppTeamMembers.add(otm);
                        }
                        //ho2 not null
                        if (l.Homeowner_2_Last_Name__c != null) {
                            Contact cnt = new Contact();
                            cnt.LastName = l.Homeowner_2_Last_Name__c;
                            cnt.AccountId = opp.AccountId;
                            cnt.Alternate_Email__c = l.Homeowner_2_Alternate_Email__c;
                            cnt.Email = l.Homeowner_2_Email__c;
                            cnt.FirstName = l.Homeowner_2_First_Name__c;
                            cnt.MobilePhone = l.Homeowner_2_Phone__c;
                            cnt.Opportunity__c = opp.Id;
                            cnt.Role__c = 'Homeowner 2';
                            contactsToAdd.add(cnt);
                        } // ho2 lastname Blank
                        if (l.Homeowner_2_First_Name__c != null && l.Homeowner_2_Last_Name__c == null) {
                            Contact cnt1 = new Contact();
                            cnt1.LastName = l.LastName;
                            cnt1.AccountId = opp.AccountId;
                            cnt1.Alternate_Email__c = l.Homeowner_2_Alternate_Email__c;
                            cnt1.Email = l.Homeowner_2_Email__c;
                            cnt1.FirstName = l.Homeowner_2_First_Name__c;
                            cnt1.MobilePhone = l.MobilePhone;
                            cnt1.Opportunity__c = opp.Id;
                            cnt1.Role__c = 'Homeowner 2';
                            contactsToAdd.add(cnt1);
                        }
                        break;
                    }
                }
                 
            }
            // Converted Contact HO1 
            contactsToUpdate = [SELECT Id from Contact where Id IN : cntIds];
            for (Contact c : contactsToUpdate) {
            	c.Role__c = 'Homeowner 1';
                for (Opportunity opp: oppsToUpdate) {
                    if (c.Id == oppAndContactMap.get(opp.Id)) {
                        c.Opportunity__c = opp.Id;
                        break;
                    }
                }
                for (Lead l: leadToUpdate) {
                    if (c.Id == l.ConvertedContactId) {
                        c.Alternate_Email__c = l.Alternate_Email__c;
                        break;
                    }
                }
            }
            // Task Uploads
            List<Task_Upload__c> taskUploads = [SELECT Id, Lead__c, Opportunity__c from Task_Upload__c WHERE Lead__c IN : leadsAndOppsMap.keySet()];
            for (Task_Upload__c tu : taskUploads) {
                tu.Opportunity__c = leadsAndOppsMap.get(tu.Lead__c);
            }
            
            try {
                update oppsToUpdate;
            } catch (DMLException e) {
                System.debug('Opportunity update in Lead Convert Failed in AutoConvertLead class' + e.getMessage());
            }
            try {
                update leadToUpdate;
            } catch (DMLException e) {
                System.debug('Lead update in Lead Convert Failed in AutoConvertLead class' + e.getMessage());
            }
            try {
                update contactsToUpdate;
            } catch (DMLException e) {
                System.debug('Contact update in Lead Convert Failed in AutoConvertLead class' + e.getMessage());
            }
             try {
                update taskUploads;
            } catch (DMLException e) {
                System.debug('taskUploads update in Lead Convert Failed in AutoConvertLead class' + e.getMessage());
            }
            try {
                insert oppTeamMembers;
            } catch (DMLException e) {
                System.debug('Team Members insert in Lead Convert Failed in AutoConvertLead class' + e.getMessage());
            }
            try {
                insert contactsToAdd;
            } catch (DMLException e) {
                System.debug('Contacts insert in Lead Convert Failed in AutoConvertLead class' + e.getMessage());
            }
            
			// Move SA from converted Lead to the new Opportunity == Service Appointements node in PB            
            List<ServiceAppointment> sapps = [SELECT Id, Lead__c, Opportunity__c from ServiceAppointment WHERE Lead__c IN : leadsAndOppsMap.keySet()];
            System.debug('OVIDIUUUUUUUUUUUUUU sapps size:' + sapps.size());
            for (ServiceAppointment sap : sapps) {
                System.debug('OVIDIUUUUUUUUUUUUUU sap:' + sap.id);
                System.debug('OVIDIUUUUUUUUUUUUUU sapLead:' + sap.Lead__c);
                System.debug('OVIDIUUUUUUUUUUUUUU sapOpp:' + sap.Opportunity__c);
                sap.Opportunity__c = leadsAndOppsMap.get(sap.Lead__c);
                sap.Lead__c = null;
            }
            
            try {
                update sapps;
            } catch (DMLException e) {
                System.debug('SA update Failed in AutoConvertLead class' + e.getMessage());
            }
            
            // Work Orders Node
            List<WorkOrder> wos = [SELECT Id, Lead__c, Opportunity__c from WorkOrder WHERE Lead__c IN : leadsAndOppsMap.keySet()];
            for (WorkOrder wo : wos) {
                wo.Opportunity__c = leadsAndOppsMap.get(wo.Lead__c);
                wo.Lead__c = null;
            }
            
            try {
                update wos;
            } catch (DMLException e) {
                System.debug('WO update Failed in AutoConvertLead class' + e.getMessage());
            }
            
            // Events Node
			List<Event> evs = [SELECT Id, Lead__c, Opportunity__c from Event WHERE Lead__c IN : leadsAndOppsMap.keySet()];
            for (Event e : evs) {
                e.Opportunity__c = leadsAndOppsMap.get(e.Lead__c);
                e.Lead__c = null;
            }
            
            try {
                update evs;
            } catch (DMLException e) {
                System.debug('Event update Failed in AutoConvertLead class' + e.getMessage());
            }            
        }    
    } 
}