public class newContactRoleOppController {
    
    public Opportunity myOpportunity {get; set;}
    public Contact myContactwRole {get; set;}
    public OpportunityContactRole myContactRole {get; set;}
    
    public newContactRoleOppController(ApexPages.StandardController controller) {
        myOpportunity = (Opportunity) controller.getRecord();
        myOpportunity = [SELECT Id, Install_Address__c, StageName, kW_Used__c, Homeowner_1__c,
						Number_of_Layers__c, Finance_Recommendation__c, Estimated_Interest_Rate__c,
						LeadSource, Utility_Provider__c, Account_Number__c, Number_of_Family_Members__c,
						Household_Income__c, Design_Aurora_Production__c, AccountId, Name,
						Design_Panel_Count__c, Design_System_Size__c, Estimated_1_Year_Production_kWh__c,
						Estimated_Annual_Electric_Usage_kWh__c, Average_TSRF__c, Estimated_Bill_Credits__c,
						Offset__c, Roof_replacement_additional_quote__c, Upgrade_Note__c, Primary_Loan_Amount1__c,
						Secondary_Loan_Amount1__c, Total_Contract_Amount1__c
                  		FROM Opportunity
                        WHERE Id = :myOpportunity.Id LIMIT 1];
        myContactwRole = new Contact();
        myContactRole = new OpportunityContactRole();
        myContactwRole.AccountId = myOpportunity.AccountId;
    }
    
    public PageReference save() {      
        try {
            myContactwRole.Role__c = myContactRole.Role;
            insert myContactwRole;
            try {
            	myContactRole.ContactId = myContactwRole.Id;
        		myContactRole.OpportunityId = myOpportunity.Id;
            	myContactRole.isPrimary = false;
        		insert myContactRole;
                return this.cancel();
            	} catch (DmlException e) {
                	System.debug('Contact ROLE Update Failed: ' + e.getMessage());
                    delete myContactwRole;
            	}
            } catch (DmlException e) {
                System.debug('Contact Update Failed: ' + e.getMessage());
            } finally {
                myContactwRole = new Contact();
                myContactRole = new OpportunityContactRole();
                myContactwRole.AccountId = myOpportunity.AccountId;
            }
        return null;
    }
    
    public PageReference cancel() {
        
        	String sServerName = ApexPages.currentPage().getHeaders().get('Host');
            sServerName = 'https://' + sServerName + '/';

            System.debug('REDIRECTING TO:'+sServerName+this.myOpportunity.Id);
            PageReference newPage =  new pagereference(sServerName+this.myOpportunity.Id);
              
            newPage.setRedirect(true);

            return newPage;
        
    }
        
}