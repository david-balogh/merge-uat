@isTest
public class TestContactController {
    
   static testMethod void testGoToOpp() {
    	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.ContactRedirectPage;
       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        
       	// creating the WO for the parentid
       	WorkOrder wo = new WorkOrder();
        wo.Earliest_Start_Permitted__c = Datetime.now();
       	wo.Opportunity__c = oppToCreate.Id;
		insert wo;        
        //create Contact and link
      	Contact contactToCreate = new Contact();
        contactToCreate.FirstName = 'TEST';
        contactToCreate.LastName = 'Testing';
        contactToCreate.OwnerId = userToCreate.id;
        contactToCreate.Opportunity__c = oppToCreate.Id;
        insert contactToCreate;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', contactToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(contactToCreate);
        ContactPageController myContr = new ContactPageController(sc);
    	myContr.goToOpportunity();
    	Test.stopTest();
   }
}