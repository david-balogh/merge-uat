@isTest
public class TestContactRolesTrigger {
	 @isTest static void TestAdd1ContactRoleHomeowner() {
        User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];

       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        // create contact
        Contact cnt =  new Contact();
        cnt.AccountId = acc.id;
        cnt.FirstName = 'xyzz';
        cnt.LastName = 'abc';
        cnt.Phone = '1234567890';
        cnt.Email = 'test@testing.com';
        cnt.Opportunity__c = oppToCreate.Id;
        insert cnt;
        OpportunityContactRole ocr = new OpportunityContactRole();
        ocr.ContactId = cnt.Id;
        ocr.Role = 'Homeowner 1';
        ocr.OpportunityId = oppToCreate.Id;
        // Perform test
        Test.startTest();
        Database.SaveResult result = Database.insert(ocr, false);
        System.assert(true, result.isSuccess());
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        //System.assert(!result.isSuccess());
        
        //System.assert(result.getErrors().size() > 0);
        //System.assertEquals('Cannot delete account with related opportunities.',
                             //result.getErrors()[0].getMessage());
    }
    @isTest static void TestAdd1ContactRoleHomeownerdoubled() {
        User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];

       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        // create contact
        Contact cnt =  new Contact();
        cnt.AccountId = acc.id;
        cnt.FirstName = 'xyzz';
        cnt.LastName = 'abc';
        cnt.Phone = '1234567890';
        cnt.Email = 'test@testing.com';
        cnt.Opportunity__c = oppToCreate.Id;
        insert cnt;
        OpportunityContactRole ocr = new OpportunityContactRole();
        ocr.ContactId = cnt.Id;
        ocr.Role = 'Homeowner 1';
        ocr.OpportunityId = oppToCreate.Id;
        insert ocr;
        
        //create contact 2
        Contact cnt2 =  new Contact();
        cnt2.AccountId = acc.id;
        cnt2.FirstName = 'xyzghyz';
        cnt2.LastName = 'absdc';
        cnt2.Phone = '1234467890';
        cnt2.Email = 'test2@testing.com';
        cnt2.Opportunity__c = oppToCreate.Id;
        insert cnt2;
        OpportunityContactRole ocr2 = new OpportunityContactRole();
        ocr2.ContactId = cnt2.Id;
        ocr2.Role = 'Homeowner 1';
        ocr2.OpportunityId = oppToCreate.Id;
        // Perform test
        Test.startTest();
        Database.SaveResult result = Database.insert(ocr2, false);
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assert(!result.isSuccess());
        System.assert(result.getErrors().size() > 0);
        System.assertEquals('You can have just one Homeowner 1 and one Homeowner 2 Contact Roles per Opportunity',
                             result.getErrors()[0].getMessage());
        Test.stopTest();
    }
    
    @isTest static void TestAdd1ContactRoleHomeownerdoubled2() {
        User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];

       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        // create contact
        Contact cnt =  new Contact();
        cnt.AccountId = acc.id;
        cnt.FirstName = 'xyzz';
        cnt.LastName = 'abc';
        cnt.Phone = '1234567890';
        cnt.Email = 'test@testing.com';
        cnt.Opportunity__c = oppToCreate.Id;
        insert cnt;
        OpportunityContactRole ocr = new OpportunityContactRole();
        ocr.ContactId = cnt.Id;
        ocr.Role = 'Homeowner 2';
        ocr.OpportunityId = oppToCreate.Id;
        insert ocr;
        
        //create contact 2
        Contact cnt2 =  new Contact();
        cnt2.AccountId = acc.id;
        cnt2.FirstName = 'xyzghyz';
        cnt2.LastName = 'absdc';
        cnt2.Phone = '1234467890';
        cnt2.Email = 'test2@testing.com';
        cnt2.Opportunity__c = oppToCreate.Id;
        insert cnt2;
        OpportunityContactRole ocr2 = new OpportunityContactRole();
        ocr2.ContactId = cnt2.Id;
        ocr2.Role = 'Homeowner 2';
        ocr2.OpportunityId = oppToCreate.Id;
        // Perform test
        Test.startTest();
        Database.SaveResult result = Database.insert(ocr2, false);
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assert(!result.isSuccess());
        System.assert(result.getErrors().size() > 0);
        System.assertEquals('You can have just one Homeowner 1 and one Homeowner 2 Contact Roles per Opportunity',
                             result.getErrors()[0].getMessage());
        Test.stopTest();
    }
	@isTest static void TestAdd1ContactRoleHomeownerdoubled3() {
        User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];

       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        // create contact
        Contact cnt =  new Contact();
        cnt.AccountId = acc.id;
        cnt.FirstName = 'xyzz';
        cnt.LastName = 'abc';
        cnt.Phone = '1234567890';
        cnt.Email = 'test@testing.com';
        cnt.Opportunity__c = oppToCreate.Id;
        insert cnt;
        OpportunityContactRole ocr = new OpportunityContactRole();
        ocr.ContactId = cnt.Id;
        ocr.Role = 'Homeowner 1';
        ocr.OpportunityId = oppToCreate.Id;
        
        //create contact 2
        Contact cnt2 =  new Contact();
        cnt2.AccountId = acc.id;
        cnt2.FirstName = 'xyzghyz';
        cnt2.LastName = 'absdc';
        cnt2.Phone = '1234467890';
        cnt2.Email = 'test2@testing.com';
        cnt2.Opportunity__c = oppToCreate.Id;
        insert cnt2;
        OpportunityContactRole ocr2 = new OpportunityContactRole();
        ocr2.ContactId = cnt2.Id;
        ocr2.Role = 'Homeowner 2';
        ocr2.OpportunityId = oppToCreate.Id;
        List<OpportunityContactRole> ocrs = new List<OpportunityContactRole>();
        ocrs.add(ocr);
        ocrs.add(ocr2);
        // Perform test
        Test.startTest();
        List<Database.SaveResult> result = Database.insert(ocrs, false);
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assert(!result[0].isSuccess());
        System.assert(result[0].getErrors().size() > 0);
        System.assertEquals('Please Insert ONLY ONE Contact Role at a time',
                             result[0].getErrors()[0].getMessage());
        Test.stopTest();
    }
    
    @isTest static void TestUpdateContactRoleHomeownerdoubled() {
        User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];

       	// create acc
       	Account acc = new Account();
        acc.Name = 'TEST NAME';
       	insert acc; 
       // create opp
		Opportunity oppToCreate = new Opportunity();
    	oppToCreate.ownerid= userToCreate.id;
    	oppToCreate.StageName ='Design';
        oppToCreate.Name = acc.Name;
    	oppToCreate.AccountId=acc.id;
    	oppToCreate.CloseDate= Date.today() + 1;
    	oppToCreate.PM_Lead__c =userToCreate.id;
    	oppToCreate.Ops_Lead__c = userToCreate.id;
    	oppToCreate.Design_Lead__c =  userToCreate.id;
    	insert oppToCreate;
        // create contact
        Contact cnt =  new Contact();
        cnt.AccountId = acc.id;
        cnt.FirstName = 'xyzz';
        cnt.LastName = 'abc';
        cnt.Phone = '1234567890';
        cnt.Email = 'test@testing.com';
        cnt.Opportunity__c = oppToCreate.Id;
        insert cnt;
        OpportunityContactRole ocr = new OpportunityContactRole();
        ocr.ContactId = cnt.Id;
        ocr.Role = 'Homeowner 1';
        ocr.OpportunityId = oppToCreate.Id;
        insert ocr;
        
        //create contact 2
        Contact cnt2 =  new Contact();
        cnt2.AccountId = acc.id;
        cnt2.FirstName = 'xyzghyz';
        cnt2.LastName = 'absdc';
        cnt2.Phone = '1234467890';
        cnt2.Email = 'test2@testing.com';
        cnt2.Opportunity__c = oppToCreate.Id;
        insert cnt2;
        OpportunityContactRole ocr2 = new OpportunityContactRole();
        ocr2.ContactId = cnt2.Id;
        ocr2.Role = 'Homeowner 2';
        ocr2.OpportunityId = oppToCreate.Id;
        insert ocr2;
            
        ocr2.Role = 'Homeowner 1';
        // Perform test
        Test.startTest();
        Database.SaveResult result = Database.update(ocr2, false);
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assert(!result.isSuccess());
        System.assert(result.getErrors().size() > 0);
        System.assertEquals('You can have just one Homeowner 1 and one Homeowner 2 Contact Roles per Opportunity',
                             result.getErrors()[0].getMessage());
        Test.stopTest();
    }
}