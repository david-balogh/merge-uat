public class TaskStopTriggerHandler {
	public List<Task> newList = new List<Task>();
    public List<Task> oldList = new List<Task>();
    public Map<Id, Task> newMap = new Map<Id, Task>();
    public Map<Id, Task> oldMap = new Map<Id, Task>();
    
    String stopTypeHard = 'Hard';
    String stopTypeMedium = 'Medium';
    String stopTypeSoft = 'Soft';
    String taskStatusCompleted = 'Completed';
    

    String projectObjName = SObjectType.Project__c.getName();    
    String permitObjName = SObjectType.Permit__c.getName();
    String designObjName = SObjectType.Design__c.getName();
    String financingObjName = SObjectType.Financing__c.getName();
    

    List<Task> taskWithHardStop = new List<Task>();
    List<Task> taskUpdatedToClosed = new List<Task>();
    List<Task> updatedTaskWithMedium = new List<Task>();
	List<Task> taskUpdatedToSoft = new List<Task>();
    
    public TaskStopTriggerHandler(List<Task> triggerNew, List<Task> triggerOld, Map<Id,Task> triggerNewMap, Map<Id,Task> triggerOldMap) {
        System.debug('TaskStopTriggerHandler!');
        this.newList = triggerNew;
        this.oldList = triggerOld;
        this.newMap  = triggerNewMap;
        this.oldMap  = triggerOldMap;
    }
    
    public void onAfterInsert(){
        try{
            classifyTaskRecords();
            // Case: When any of the 4 child objects of Opportunity have a task that is created or updated, where Stop_Type__c = Hard AND IsClosed != TRUE
            if(taskWithHardStop.size() > 0){
                processHardTaskWithNoClosed();
            }
        }catch(Exception ex){
            System.debug('** Error onAfterInsert ' + ex.getMessage() + ' at line ' + ex.getLineNumber());
        }
        
    }
    
    public void onAfterUpdate(){
        try{
            classifyTaskRecords();
            // Case: When any of the 4 child objects of Opportunity have a task that is created or updated, where Stop_Type__c = Hard AND IsClosed != TRUE
            if(taskWithHardStop.size() > 0){
                processHardTaskWithNoClosed();
            }
            
            // Case: When a Task with a Stop_Type__c = Hard has its IsClosed value changed to TRUE
            if(taskUpdatedToClosed.size() > 0){
                processHardTaskUpdatedToClosed();
            }
            
            // Case: When a Task with a Stop_Type__c = Medium is updated
            if(updatedTaskWithMedium.size() > 0){
                processUpdatedTaskWithMedium();
            }
            
            // Case: If a task is updated, and afterward no task records are then returned where Stop_Type__c = Hard || Medium AND IsClosed != TRUE
            if(taskUpdatedToSoft.size() > 0){
                processUpdatedTaskWithSoft();
            }
        }catch(Exception ex){
            System.debug('** Error onAfterUpdate ' + ex.getMessage() + ' at line ' + ex.getLineNumber());
        }        
    }
    
    // 1st specs block
    private void processHardTaskWithNoClosed(){
        System.debug('processHardTaskWithNoClosed');        
        Set<Id> oppIds = new Set<Id>();
        // filter which task record has opportunity Id
        for(Task task : taskWithHardStop){
            if(task.Opportunity__c != null){
                oppIds.add(task.Opportunity__c);                
            }
        }            
        
        if(oppIds.size() > 0){
            List<Opportunity> updatedOpps = [SELECT Id, Stops__c FROM Opportunity WHERE Id IN : oppIds];
            for(Opportunity opp : updatedOpps){
                opp.Stops__c = stopTypeHard;
            }
            
            if(updatedOpps.size() > 0){
                update updatedOpps;
            }
        }
    }
    
    // 2nd specs block
    private void processHardTaskUpdatedToClosed(){
        System.debug('processHardTaskUpdatedToClosed');
        Map<Id, List<Task>> oppTaskMap = new Map<Id, List<Task>>();
        Set<Id> oppIds = new Set<Id>();
        Set<Id> taskIds = new Set<Id>();
        Map<Id, Double> mediumOpps = new Map<Id, Double>();
        Set<Id> softOpps = new Set<Id>();
        // filter which task record has opportunity Id
        for(Task task : taskUpdatedToClosed){
            if(task.Opportunity__c != null){
                oppIds.add(task.Opportunity__c);
                taskIds.add(task.Id);
                oppTaskMap.put(task.Opportunity__c, new List<Task>());
            }
        }        
        if(oppIds.size() > 0){
            // query all tasks with the same parent opportunity to determine if any other Hard or Medium stops remain
            List<Task> queriedTasks = [SELECT Id, Stop_Type__c, IsClosed, Status, Opportunity__c, Stop_On_Stage_Number__c 
                                       FROM Task 
                                       WHERE Opportunity__c IN :oppIds];// AND Id NOT IN :taskIds];
            for(Task queriedTask : queriedTasks){
                if(oppTaskMap.containsKey(queriedTask.Opportunity__c)){
                    oppTaskMap.get(queriedTask.Opportunity__c).add(queriedTask);
                }else{
                    oppTaskMap.put(queriedTask.Opportunity__c, new List<Task>{queriedTask});
                }
            }
        }
        
        for(Id oppId : oppTaskMap.keySet()){
            Boolean hasNoneClosedTaskWithStopHard = false;
            Boolean hasInCompletedTaskWithStopMedium = false;
            Boolean hasNoneClosedTaskWithStopHardOrMedium = false;
            List<Task> mediumTasks = new List<Task>();
            for(Task task : oppTaskMap.get(oppId)){
                if(task.Stop_Type__c == stopTypeHard && task.IsClosed != true){
                    hasNoneClosedTaskWithStopHard = true;
                }
                
                if(task.Stop_Type__c == stopTypeMedium && task.Status != taskStatusCompleted){
                    hasInCompletedTaskWithStopMedium = true;
                    mediumTasks.add(task);
                }
                
                if((task.Stop_Type__c == stopTypeHard || task.Stop_Type__c == stopTypeMedium) && task.IsClosed != true){
                    hasNoneClosedTaskWithStopHardOrMedium = true;
                }
            }
            
            if(hasNoneClosedTaskWithStopHard == false){
                if(hasInCompletedTaskWithStopMedium == true){
                    Double minStopOnStageNumber = mediumTasks[0].Stop_On_Stage_Number__c;
                    for(Task task : mediumTasks){
                        if(task.Stop_On_Stage_Number__c < minStopOnStageNumber){
                            minStopOnStageNumber = task.Stop_On_Stage_Number__c;
                        }
                    }
                    mediumOpps.put(oppId, minStopOnStageNumber);   
                }
                
                if(hasNoneClosedTaskWithStopHardOrMedium == false){
                    softOpps.add(oppId);
                }
            }
        }
        
        List<Opportunity> updatedOpps = new List<Opportunity>();
        List<Opportunity> updatedMediumOpps = [SELECT Id, Stops__c, Lowest_Medium_Stop_Stage_Number__c 
                                              FROM Opportunity 
                                              WHERE Id IN : mediumOpps.keySet()];
        for(Opportunity opp : updatedMediumOpps){
            opp.Stops__c = stopTypeMedium;
            opp.Lowest_Medium_Stop_Stage_Number__c = mediumOpps.get(opp.Id);
        }
        updatedOpps.addAll(updatedMediumOpps);
        
        List<Opportunity> updatedSoftOpps = [SELECT Id, Stops__c, Lowest_Medium_Stop_Stage_Number__c 
                                             FROM Opportunity 
                                             WHERE Id IN : softOpps];
        for(Opportunity opp : updatedSoftOpps){
            opp.Stops__c = stopTypeSoft;
            opp.Lowest_Medium_Stop_Stage_Number__c = null;            
        }
        updatedOpps.addAll(updatedSoftOpps);
        
        if(updatedOpps.size() > 0){
            update updatedOpps;
        }
        
    }
    
    // 3rd specs block
    private void processUpdatedTaskWithMedium(){
        System.debug('processUpdatedTaskWithMedium');
        Map<Id, List<Task>> oppTaskMap = new Map<Id, List<Task>>();
        Set<Id> oppIds = new Set<Id>();
        Set<Id> taskIds = new Set<Id>();
        Map<Id, Double> mediumOpps = new Map<Id, Double>();
        Set<Id> hardOpps = new Set<Id>();
        System.debug('Updated Task Size ' + updatedTaskWithMedium.size());
        for(Task task : updatedTaskWithMedium){
            System.debug('Updated Task Sub ' + task.Subject);
            if(task.Opportunity__c != null){
                oppIds.add(task.Opportunity__c);
                taskIds.add(task.Id);
                oppTaskMap.put(task.Opportunity__c, new List<Task>());
            }
        }
        System.debug('oppIds size ' + oppIds.size());
        if(oppIds.size() > 0){
            // query all tasks with the same parent opportunity to determine if any other Hard or Medium stops remain
            List<Task> queriedTasks = [SELECT Id, Stop_Type__c, IsClosed, Status, Opportunity__c, Stop_On_Stage_Number__c, Subject 
                                       FROM Task 
                                       WHERE Opportunity__c IN :oppIds 
                                       AND (Stop_Type__c = :stopTypeHard OR Stop_Type__c = :stopTypeMedium)];
                                       //AND Id NOT IN :taskIds];
            System.debug('queriedTasks size ' + queriedTasks.size());
            for(Task queriedTask : queriedTasks){
                System.debug('Task Sub ' + queriedTask.Subject);
                if(oppTaskMap.containsKey(queriedTask.Opportunity__c)){
                    oppTaskMap.get(queriedTask.Opportunity__c).add(queriedTask);
                }else{
                    oppTaskMap.put(queriedTask.Opportunity__c, new List<Task>{queriedTask});
                }
            }
            
            for(Id oppId : oppTaskMap.keySet()){
                Boolean hasClosedTaskWithStopHard = false;
                Boolean hasClosedTaskWithStopMedium = false;
                List<Task> mediumTasks = new List<Task>();
                for(Task task : oppTaskMap.get(oppId)){
                    if(task.Stop_Type__c == stopTypeHard && task.IsClosed != true){
                        hasClosedTaskWithStopHard = true;
                    }
                    
                    if(task.Stop_Type__c == stopTypeMedium && task.IsClosed != true){
                        hasClosedTaskWithStopMedium = true;
                        mediumTasks.add(task);
                    }
                }
                
                if(hasClosedTaskWithStopHard){
                    hardOpps.add(oppId);
                }else if(hasClosedTaskWithStopMedium){
                    Double minStopOnStageNumber = mediumTasks[0].Stop_On_Stage_Number__c;
                    for(Task task : mediumTasks){
                        if(task.Stop_On_Stage_Number__c < minStopOnStageNumber){
                            minStopOnStageNumber = task.Stop_On_Stage_Number__c;
                        }
                    }
                    mediumOpps.put(oppId, minStopOnStageNumber);
                }
            }
            
            List<Opportunity> updatedOpps = new List<Opportunity>();
            List<Opportunity> updatedMediumOpps = [SELECT Id, Stops__c, Lowest_Medium_Stop_Stage_Number__c 
                                                   FROM Opportunity 
                                                   WHERE Id IN : mediumOpps.keySet()];
            for(Opportunity opp : updatedMediumOpps){
                opp.Stops__c = stopTypeMedium;
                opp.Lowest_Medium_Stop_Stage_Number__c = mediumOpps.get(opp.Id);
            }
            updatedOpps.addAll(updatedMediumOpps);
            
            List<Opportunity> updatedHardOpps = [SELECT Id, Stops__c 
                                                 FROM Opportunity 
                                                 WHERE Id IN : hardOpps];
            for(Opportunity opp : updatedHardOpps){
                opp.Stops__c = stopTypeHard;            
            }
            updatedOpps.addAll(updatedHardOpps);
            
            if(updatedOpps.size() > 0){
                update updatedOpps;
            }
        }
        
    }
    
    // 4th specs block
    private void processUpdatedTaskWithSoft(){
        System.debug('processUpdatedTaskWithSoft');
        Map<Id, List<Task>> oppTaskMap = new Map<Id, List<Task>>();
        Set<Id> oppIds = new Set<Id>();
        Set<Id> softOpps = new Set<Id>();
        for(Task task : taskUpdatedToSoft){
            if(task.Opportunity__c != null){
                oppIds.add(task.Opportunity__c);
                oppTaskMap.put(task.Opportunity__c, new List<Task>());
            }
        }
        
        if(oppIds.size() > 0){
            // query all tasks with the same parent opportunity to determine if any other Hard or Medium stops remain
            List<Task> queriedTasks = [SELECT Id, Stop_Type__c, IsClosed, Opportunity__c 
                                       FROM Task 
                                       WHERE Opportunity__c IN :oppIds];
            for(Task queriedTask : queriedTasks){
                if(oppTaskMap.containsKey(queriedTask.Opportunity__c)){
                    oppTaskMap.get(queriedTask.Opportunity__c).add(queriedTask);
                }else{
                    oppTaskMap.put(queriedTask.Opportunity__c, new List<Task>{queriedTask});
                }
            }
            
            for(Id oppId : oppTaskMap.keySet()){
                Boolean hasNoClosedTaskWithStopHardOrMedium = false;
                for(Task task : oppTaskMap.get(oppId)){
                    system.debug('task.Stop_Type__c Soft ' + task.Stop_Type__c);
                    if((task.Stop_Type__c == stopTypeHard || task.Stop_Type__c == stopTypeMedium) && task.IsClosed != true){
                        hasNoClosedTaskWithStopHardOrMedium = true;
                    }
                }
                
                if(hasNoClosedTaskWithStopHardOrMedium == false){
                    softOpps.add(oppId);
                }
            }
            List<Opportunity> updatedOpps = new List<Opportunity>();
            List<Opportunity> updatedSoftOpps = [SELECT Id, Stops__c, Lowest_Medium_Stop_Stage_Number__c 
                                                 FROM Opportunity 
                                                 WHERE Id IN : softOpps];
            for(Opportunity opp : updatedSoftOpps){
                opp.Stops__c = stopTypeSoft;
                opp.Lowest_Medium_Stop_Stage_Number__c = null;
            }
            updatedOpps.addAll(updatedSoftOpps);
            
            if(updatedOpps.size() > 0){
                update updatedOpps;
            }
            
        }
    }
    
    private void classifyTaskRecords(){   
        System.debug('** Trigger Type: ' + (oldMap?.size() > 0 ? 'UPDATE' : 'INSERT'));
        system.debug('** Trigger Size: ' + newList.size());        
        for(Task task : newList){            
            System.debug('** Task Id: ' + task.Id);
            System.debug('** Stop Type: ' + task.Stop_Type__c);
            System.debug('** Stop IsClosed: ' + task.IsClosed);
            System.debug('** Task Parent Id: ' + task.WhatId);
            System.debug('** Task Parent Type: ' + task.WhatId?.getSObjectType());
			// When any of the 4 child objects of Opportunity have a task that is created or updated, where Stop_Type__c = Hard AND IsClosed != TRUE            
            if(task.Stop_Type__c == stopTypeHard && task.IsClosed != true){
                String parentObjName = task.WhatId?.getSObjectType().getDescribe().getName();
                if(parentObjName == projectObjName || parentObjName == permitObjName || parentObjName == designObjName || parentObjName == financingObjName){
                    taskWithHardStop.add(task);
                }                                 
            }
            
            // When a Task with a Stop_Type__c = Hard has its IsClosed value changed to TRUE
            if(task.Stop_Type__c == stopTypeHard && task.IsClosed == true && oldMap?.size() > 0){
                if(oldMap.containsKey(task.Id) && oldMap.get(task.Id).IsClosed != true){
                    taskUpdatedToClosed.add(task);    
                }                
            }
            
            // When a Task with a Stop_Type__c = Medium is updated
            if(task.Stop_Type__c == stopTypeMedium && oldMap?.size() > 0){
                updatedTaskWithMedium.add(task);
            }            
            
            // If a task is updated, and afterward no task records are then returned where Stop_Type__c = Hard || Medium AND IsClosed != TRUE
            if(((task.Stop_Type__c != stopTypeHard && task.Stop_Type__c != stopTypeMedium) || task.IsClosed) && oldMap?.size() > 0){
                taskUpdatedToSoft.add(task);
            }
			            
        }        
        
    }
}