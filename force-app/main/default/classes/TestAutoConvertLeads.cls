@isTest
public class TestAutoConvertLeads {
	static testMethod void createnewlead() {
    User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
     
    Test.startTest();    
    Lead leadToCreate =new Lead();
    List<id> Ids= New List<Id>();
    leadToCreate.ownerid= userToCreate.id;
    leadToCreate.LastName ='Gupta';
    leadToCreate.Company='Salesforce';
    leadToCreate.LeadSource='Partner Referral';
    leadToCreate.Rating='';
    leadToCreate.Status='';
    leadToCreate.Street = '463 Lafayette Ave';
    leadToCreate.City = 'Brooklyn';
    leadToCreate.State = 'NY';
    leadToCreate.PostalCode = '11205';
    insert leadToCreate;
    //create WO
    WorkOrder woToCreate = new WorkOrder();
    insert woToCreate;    
    //Create SA    
    ServiceAppointment saToCreate = new ServiceAppointment();
    saToCreate.Status = 'Scheduled';
    saToCreate.OwnerId = userToCreate.id;
    String inputString = '2031-02-06T22:45:00.000Z';
	DateTime resultDateTime = DateTime.ValueofGmt(inputString.replace('T', ' '));
	String inputString1 = '2031-02-01T22:45:00.000Z';
	DateTime resultDateTime1 = DateTime.ValueofGmt(inputString1.replace('T', ' '));
    saToCreate.DueDate = resultDateTime;
    saToCreate.EarliestStartTime = resultDateTime1;
    saToCreate.Lead__c = leadToCreate.Id;
    saToCreate.Opportunity__c = null;
    saToCreate.ParentRecordId = woToCreate.Id;
    insert saToCreate;
        
    Ids.add(leadToCreate.id);
    AutoConvertLeads.LeadAssign(Ids);
      
    Test.stopTest();
   }
}