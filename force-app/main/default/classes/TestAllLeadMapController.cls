@isTest
public class TestAllLeadMapController {
	static testMethod void testSwitch() {
    	User userToCreate = [Select id from user where profile.name='System Administrator' and IsActive = true Limit 1];
      
      	Test.startTest();
      	PageReference pageRef = Page.allLeadMap;
        // create lead 
		Lead leadToCreate = new Lead();
    	LeadToCreate.ownerid= userToCreate.id;
    	leadToCreate.LastName ='Gupta';
    	leadToCreate.Company='Salesforce';
    	leadToCreate.LeadSource='Partner Referral';
    	leadToCreate.Rating='';
    	leadToCreate.Status='';
    	leadToCreate.Street = '463 Lafayette Ave';
    	leadToCreate.City = 'Brooklyn';
    	leadToCreate.State = 'NY';
    	leadToCreate.PostalCode = '11205';
    	insert leadToCreate;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', leadToCreate.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(leadToCreate);
        AllLeadMapController myContr = new AllLeadMapController(sc);
    	myContr.switch();
    	Test.stopTest();
   }
}