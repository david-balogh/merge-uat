trigger CountProjectTasksTrigger on Project__c (before update) {

    // Put all Projects into a Set
    Set<Id> allProjIDs = new Set<Id>();

    for (Project__c newProj : Trigger.new) {
        allProjIDs.add(newProj.ID);
        System.debug('ID added: ' + newProj.ID);
    }
    
    //Query All Tasks to find Tasks with matching IDs
    List<Task> allTasks = [SELECT Id, Status, WhatID FROM Task
                           WHERE WhatID IN :allProjIDs];
    System.debug('allTasks is ' + allTasks.size());
    // Create a Map that lets you search for Tasks by their ID - faster than SOQL each time
    
    List<Id> TaskArray = new List<Id>(); //All Tasks
    List<Id> IDToTaskMapOpen = new List<Id>(); //usedforOpenTasks
    List<Id> IDToTaskMapClosed = new List<Id>(); //usedforClosedTasks
    
    Map<Id, Integer> elCountAll = new Map<Id, Integer>(); //Count of All Tasks
    Map<Id, Integer> elCountOpen = new Map<Id, Integer>(); //Count of Open Tasks
    Map<Id, Integer> elCountClosed = new Map<Id, Integer>(); //Count of Closed Tasks
    
    for (Task u : allTasks) {
        if (u.Status != 'Completed' && u.Status != 'Cancelled' ) {
            IDToTaskMapOpen.add(u.WhatID);
            System.debug('Added Open Task');
            
        }
        if (u.Status == 'Completed' || u.Status == 'Cancelled' ) {
            System.debug('Added Completed Task');
            IDToTaskMapClosed.add(u.WhatID);
            
        }
        TaskArray.add(u.WhatID);
    }  
    
    System.debug(allTasks.size());
    System.debug(IDToTaskMapOpen.size());
    System.debug(IDToTaskMapClosed.size());
    
    
    
    // Get the matching tasks from the Map - and count Status
    //Start with our Project
    for (Project__c newProj : Trigger.new) {
        //Count all Tasks
        for(String key : TaskArray)
        {
            if(!elCountAll.containsKey(key)){
                elCountAll.put(key,0);
            }
            Integer currentInt=elCountAll.get(key)+1;
            elCountAll.put(key,currentInt);
        }
        
        
        //Count all Open Tasks
        for(String key1 : IDToTaskMapOpen)
        {
            if(!elCountOpen.containsKey(key1)){
                elCountOpen.put(key1,0);
            }
            Integer currentInt1=elCountOpen.get(key1)+1;
            elCountOpen.put(key1,currentInt1);
        }
        
        //Count all Closed Tasks
        for(String key2 : IDToTaskMapClosed)
        {
            if(!elCountClosed.containsKey(key2)){
                elCountClosed.put(key2,0);
            }
            Integer currentInt2=elCountClosed.get(key2)+1;
            elCountClosed.put(key2,currentInt2);
        }
        
        
        //Finally update the record
        Integer sum = 0, all = 0, op = 0, cl = 0;
        all = elCountAll.get(newProj.ID);
        op = elCountOpen.get(newProj.ID);
        cl = elCountClosed.get(newProj.ID);
        // if there's nothing to count the value is set to null so I have to make sure
        // I won't have a Null Pointer Exception
        if(op == null) {
            op = 0;
        }

        if(cl == null) {
            cl = 0;
        }

        if( op == 0 && cl > 0 ) {
            newProj.Status__c = 'Completed';
        }
        // maybe I could do some logic for when the above condition is not good
        // what if I add a new task? Or delete one? Ask Ynah about it
        /*//All Activities
        newProj.AllTasks__c = elCountAll.get(newProj.ID);
        
        //Open & Not Started Tasks
        newProj.Open_Tasks__c = elCountOpen.get(newProj.ID);
        
        //Closed Tasks
        newProj.Closed_Tasks__c = elCountClosed.get(newProj.ID);*/
    }
}