trigger CompletedProjectByTaskTrigger on Task (after update) {
    List<Id> taskIds = new List<Id>();
    List<Id> projIds = new List<Id>(); 
    for (Integer i = 0; i < Trigger.new.size(); i++) {
        // we are saving the id only for changing triggers from another status to completed, these tasks should be linked to a project
        if (Trigger.new[i].WhatId != null && Trigger.new[i].WhatId.getSObjectType() == Project__c.sObjectType
             && (Trigger.new[i].Status == 'Completed' || Trigger.new[i].Status == 'Cancelled') && Trigger.old[i].Status != Trigger.new[i].Status) {
            taskIds.add(Trigger.new[i].Id);
            projIds.add(Trigger.new[i].WhatId);
        }
    }
    if(taskIds.size() > 0 && projIds.size() > 0) {
        
        List<Project__c> projects = [SELECT id, name FROM Project__c WHERE Id IN :projIds];
        update projects;
    }
}