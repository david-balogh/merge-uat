trigger CountSAppOnLeadTrigger on Lead (before update) {
    // Put all Leads into a Set
    Set<Id> allLeadIDs = new Set<Id>();

    for (Lead newLead : Trigger.new) {
        allLeadIDs.add(newLead.ID);
        System.debug('ID added: ' + newLead.ID);
    }
    
    //Query All SAs to find SAs with matching IDs
    List<ServiceAppointment> allAppointments = [SELECT Id, Status, ParentRecordId FROM ServiceAppointment
                           WHERE ParentRecordId IN :allLeadIDs];
    System.debug('allSAs is ' + allAppointments.size());
    // Create a Map that lets you search for SAs by their ID
    
    List<Id> sAppArray = new List<Id>(); //All SAs
    List<Id> IDToSAppMapOpen = new List<Id>(); //usedforOpenSAs
    List<Id> IDToSAppMapClosed = new List<Id>(); //usedforClosedSAs
    
    Map<Id, Integer> elCountAll = new Map<Id, Integer>(); //Count of All SAs
    Map<Id, Integer> elCountOpen = new Map<Id, Integer>(); //Count of Open SAs
    Map<Id, Integer> elCountClosed = new Map<Id, Integer>(); //Count of Closed SAs
    
    for (ServiceAppointment u : allAppointments) {
        if (u.Status != 'Completed' && u.Status != 'Canceled' && u.Status != 'Cannot Complete') {
            IDToSAppMapOpen.add(u.ParentRecordId);
            System.debug('Added Open SA');
            
        }
        if (u.Status == 'Completed' || u.Status == 'Canceled' || u.Status == 'Cannot Complete') {
            System.debug('Added Closed SA');
            IDToSAppMapClosed.add(u.ParentRecordId);
            
        }
        sAppArray.add(u.ParentRecordId);
    }  
    
    // Get the matching SAs from the Map - and count Status
    //Start with our Lead
    for (Lead newLead : Trigger.new) {
        //Count all Tasks
        for(String key : sAppArray)
        {
            if(!elCountAll.containsKey(key)){
                elCountAll.put(key,0);
            }
            Integer currentInt=elCountAll.get(key)+1;
            elCountAll.put(key,currentInt);
        }
        
        
        //Count all Open SAs
        for(String key1 : IDToSAppMapOpen)
        {
            if(!elCountOpen.containsKey(key1)){
                elCountOpen.put(key1,0);
            }
            Integer currentInt1=elCountOpen.get(key1)+1;
            elCountOpen.put(key1,currentInt1);
        }
        
        //Count all Closed SAs
        for(String key2 : IDToSAppMapClosed)
        {
            if(!elCountClosed.containsKey(key2)){
                elCountClosed.put(key2,0);
            }
            Integer currentInt2=elCountClosed.get(key2)+1;
            elCountClosed.put(key2,currentInt2);
        }
        
        
        //Finally update the record
        Integer sum = 0, all = 0, op = 0, cl = 0;
        all = elCountAll.get(newLead.ID);
        op = elCountOpen.get(newLead.ID);
        cl = elCountClosed.get(newLead.ID);
        // if there's nothing to count the value is set to null so I have to make sure
        // I won't have a Null Pointer Exception
        if(op == null) {
            op = 0;
        }
        
        if(all == null) {
            all = 0;
        }

        if(cl == null) {
            cl = 0;
        }

        if( op == 0 && cl == all && cl > 0) {
            newLead.AllowConversionBySA__c = true;
        }
    }
}