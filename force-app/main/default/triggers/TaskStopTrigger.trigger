trigger TaskStopTrigger on Task (after update, after insert) {
    TaskStopTriggerHandler handler = new TaskStopTriggerHandler(trigger.new, trigger.old, trigger.newMap, trigger.oldMap);
    if(Trigger.isAfter && Trigger.isInsert){
        handler.onAfterInsert();
    }
    if(Trigger.isAfter && Trigger.isUpdate){
        handler.onAfterUpdate();
    }
    
   // I'm still trying the process builder thing
   //  //?? is there any situation where we use bulk update/insert for Tasks?
   /*  if (Trigger.isInsert) {
        if (Trigger.New.size() == 1) {
            Task t = Trigger.New[0];
            Schema.SObjectType sobjectType = t.WhatId.getSObjectType();
            String sobjectName = sobjectType.getDescribe().getName();
            if (sobjectName == Opportunity.getSObjectType().getDescribe().getName()) {
                // do what you would do to aOppon opportunity
                Opportunity op = [SELECT Id FROM Opportunity WHERE Id=:t.WhatId];
            } else {
                Opportunity
            }
            
            
            
            
            
            if (t.Stop_Type__c == 'Hard') {
                String OppId = t.WhatId;
                Opportunity opp = [SELECT Id from Opportunity where Id =:OppId];
                system.debug('OVIDIU PRINT TaskStopTrigger' + opp);
                //t.Stop_on_Stage__c = null;
            }
        }
     }
     
     if (Trigger.isUpdate) {
     
    }*/

}