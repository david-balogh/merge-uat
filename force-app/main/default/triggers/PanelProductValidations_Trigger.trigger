trigger PanelProductValidations_Trigger on OpportunityLineItem (after insert, after update) {
    // this trigger can be trated better with separate conditions for insert and update
    // we need to prevent the trigger update to do unncessary work if the Number_Of_Panels
    //                  was not changed, or we could just delete the update part
    //                  talk about it after Feb 15th 
    // we will trigger the functionality only on one item updated/iserted at a time
    if (Trigger.new.size() == 1) {

        OpportunityLineItem oppProduct = Trigger.new[0];
        Product2 product2 = [SELECT id, RecordTypeId
                            FROM Product2
                            where id =: oppProduct.Product2Id];
        RecordType opptProdRecordType = [SELECT id, Name
                    FROM RecordType
                    WHERE id = : product2.RecordTypeId
                    LIMIT 1];

        if (opptProdRecordType.Name == 'Panels') {
            // retreive all product2 ids of record type 'panels'
            List<Product2> products = [SELECT id, Watts__c
                                FROM Product2
                                WHERE RecordTypeId =: opptProdRecordType.Id];
            List<Id> prodIds = new List<Id>();
            for (Product2 it : products) {
                
                prodIds.add(it.Id);
            }

            /* retreive the OpportunityLineItems per Opportunity
                the OpportunityLineItem(Product2) has to be of Type 'panels'
                We are also sorting the list by the number of panels 
            */
            List<OpportunityLineItem> productsPerOppt = [SELECT id, Product2Id,
                            OpportunityId, Multi_array__c, Number_of_Panels__c,
                            System_Size__c 
                        FROM OpportunityLineItem 
                        WHERE OpportunityId =: oppProduct.OpportunityId
                            AND Product2Id IN :prodIds
                        ORDER BY Number_of_Panels__c DESC NULLS LAST];
            // starting the automation for MVP-111
            if (productsPerOppt.size() > 2) {
                // setting all the Multi-array to true(y)
                for(OpportunityLineItem it : productsPerOppt) {
                    
                    it.Multi_array__c = true;
                }
                // updating the list with Multi-array to false(n) 
                // for the first two sorted elements of the products list
                OpportunityLineItem max_item = productsPerOppt.get(0);
                OpportunityLineItem max_item1 = productsPerOppt.get(1);
                max_item.Multi_array__c = false;
                max_item1.Multi_array__c = false;
                productsPerOppt.set(0, max_item);
                productsPerOppt.set(1, max_item1);
                update productsPerOppt;
            }

            // automation for MVP-112
            Opportunity opp = [SELECT id, Panel_Watt__c, Panel_Type__c
                                FROM Opportunity 
                                WHERE id =: oppProduct.OpportunityId LIMIT 1];
            Product2 prod_help = [SELECT Id, Name
                                FROM Product2
                                WHERE id =: productsPerOppt.get(0).Product2Id
                                LIMIT 1];
            opp.Panel_Type__c = prod_help.Name;
            // Panel Watt is calculated as the sum of Watts__c field for all
            // the Products of Record Type 'Panels', being related to the Opportunity
            Decimal watts = 0;
            List<Id> prodIdList = new List<Id>();
            for (OpportunityLineItem it : productsPerOppt) {
                prodIdList.add(it.Product2Id);
            }
            //List<Product2> productsWithDuplicate = new List<Product2>();
            for (Id i : prodIdList) {
                for (Product2 p2 : products) {
                    if (p2.Id == i) {
                        watts = watts + p2.Watts__c;
                        break;
                    }
                }
            }
            opp.Panel_Watt__c = watts;
            // calculate Opportunity System Size (rool up over System Size Opp Product)
            Decimal osSize = 0;
            for(OpportunityLineItem it : productsPerOppt) {
                osSize = osSize + it.System_Size__c;  
            }
            opp.Opportunity_System_Size__c = osSize;
            update opp;
        }
    }
}