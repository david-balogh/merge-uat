trigger ContactRoles_Trigger on OpportunityContactRole (before insert, before update) {

    if(Trigger.new.size() > 1) {
        //alert. you can insert/update just one OpportunityContactRole
        for(OpportunityContactRole c : Trigger.new) {
            c.addError('Please Insert ONLY ONE Contact Role at a time');
        }
    } else {
        OpportunityContactRole contact = Trigger.new[0];
        if (Trigger.isInsert) {
            List<OpportunityContactRole> oppContacts = [SELECT Id, Role FROM OpportunityContactRole WHERE Role = : contact.Role AND OpportunityId = :contact.OpportunityId];
            if (oppContacts.size() == 1 && (  contact.Role == 'Homeowner 1' || contact.Role == 'Homeowner 2')) {
                Trigger.new[0].adderror('You can have just one Homeowner 1 and one Homeowner 2 Contact Roles per Opportunity');
            }
        }
        if (Trigger.isUpdate) {
            if (Trigger.old[0].Role != contact.Role || Trigger.old[0].OpportunityId != contact.OpportunityId) {
                List<OpportunityContactRole> oppContacts = [SELECT Id, Role FROM OpportunityContactRole WHERE Role = : contact.Role AND OpportunityId = :contact.OpportunityId];
                if (oppContacts.size() == 1 && (  contact.Role == 'Homeowner 1' || contact.Role == 'Homeowner 2')) {
                    Trigger.new[0].adderror('You can have just one Homeowner 1 and one Homeowner 2 Contact Roles per Opportunity');
                }
            }

        }
    }
}