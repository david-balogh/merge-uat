trigger ConvertOnAssignedResourceCreationTrigger on AssignedResource (after insert) {
    
    List<Id> SAids = new List<Id>();
    for(AssignedResource res : Trigger.new) {
        SAIds.add(res.ServiceAppointmentId);        
    }
    // edited this process builder Service Appointment Insert/ Edit 
    //  fac un field pe opportunity si pe lead, pe lead il pun aici. pe opp il pun in convert method-> convert triggerd by SA id si il bag acolo
    //after canceling the SAs, we will check if we had Final Appointments and convert the according leads
    List<Id> leadsToConvertIds = new List<Id>();
    Map<Id, Id> WoAndSaMap = new Map<Id,Id>();
    Map<Id, Id> LeadAndWOMap = new Map<Id,Id>();
    for (ServiceAppointment sa : [SELECT Id, Lead__c, ParentRecordId from ServiceAppointment where Id IN :SAIds]) {
        System.debug('OVIDIUUUUUUUUUU SA parent:' + sa.ParentRecordId);
        if(sa.ParentRecordId != null) {
            WoAndSaMap.put(sa.ParentRecordId, sa.Id);
        }
    }
    
        for (WorkOrder wo : [SELECT Id, Lead__c, WorkTypeId, WorkType.Name FROM WorkOrder WHERE Id IN : WoAndSaMap.keySet()]) {
            System.debug('OVIDIUUUUUUUUUU WO Id:' + wo.Id);
            System.debug('OVIDIUUUUUUUUUU WO Lead:' + wo.Lead__c);
            System.debug('OVIDIUUUUUUUUUU SA work type:' + wo.WorkTypeId);
            
            if (wo.Lead__c != null && wo.WorkType.Name == '2. Proposal Appointment') { 
                leadsToConvertIds.add(wo.Lead__c);
                LeadAndWoMap.put(wo.Lead__c, wo.Id);
            }
        }
        
        System.debug('OVIDIUUUUUUUUUU Leads to convert Ids:' + leadsToConvertIds.size());
        List<Lead> leadsToConvert = [SELECT Id, Name, FirstName, LastName, Email, Number_of_Family_Members__c, Status,
                  Household_Income__c, kW_Used__c, Finance_Recommendation__c, Not_Interested__c,Payable_lead1__c,
                  Not_Qualified_Reason__c, Start_Conversion__c, isConverted, HasProposalApp__c,
                  Street, Homeowner_2_Email__c, Homeowner_2_First_Name__c, Country, MobilePhone,
                  Alternate_Email__c, Homeowner_2_Alternate_Email__c, City, State, PostalCode,
                  Homeowner_2_Last_Name__c, Homeowner_2_Phone__c, Disqualified_Reason__c, SATriggConvert__c
                  from Lead where Id IN :leadsToConvertIds];
        System.debug('OVIDIUUUUUUUUUU Leads to convert:' + leadsToConvert.size());
        for (Lead l : leadsToConvert) {
            l.HasProposalApp__c = true;
        }
        
        try {
            update leadsToConvert;
        } catch (DMLException e) {
            System.debug('Lead Update Failed in ConvertOnAssignedResourceCreationTrigger' + e.getMessage());
        }
}