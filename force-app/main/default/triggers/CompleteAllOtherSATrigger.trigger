trigger CompleteAllOtherSATrigger on ServiceAppointment (after insert) {

    if(Trigger.isAfter) {
        
        List<Id> oppIds = new List<Id>();
        List<Id> leadIds = new List<Id>();
        List<Id> mySAIds = new List<Id>();
        List<Id> myWOIds = new List<Id>();
        List<ServiceAppointment> leadSAs = new List<ServiceAppointment>();
        List<ServiceAppointment> oppSAs = new List<ServiceAppointment>();
    
        for (ServiceAppointment sa : Trigger.new) {
            System.debug('OVIDIUUUUUU WTId' + sa.WorkTypeId);
            System.debug('OVIDIUUUUUU ParentRTId' + sa.ParentRecordId);
            if (sa.WorkTypeId == '08q4x000000cg3IAAQ' ||
                sa.WorkTypeId == '08q4x000000cg3LAAQ' ||
                sa.WorkTypeId == '08q4x000000cg3JAAQ' ||
                sa.WorkTypeId == '08q4x000000cg3HAAQ' ||
                sa.WorkTypeId == '08q4x000000cg3KAAQ') {
                    if(sa.ParentRecordId != null /*&& sa.ParentRecordType == 'Work Order'*/) {
                        myWOIds.add(sa.ParentRecordId);
                    }
            }
            mySAIds.add(sa.Id);
        }
    
        System.debug('OVIDIUUUUUU myWOIDs size' + myWOIds.size());
        //Map to store pairs of WorkOrder(Id key) and Lead(Id value)
        Map<Id, Id> woIdandLeadId = new Map<Id, Id>();
        if (myWOIds.size() > 0) {
            List<WorkOrder> worders = [SELECT Id, Lead__c, Opportunity__c FROM WorkOrder WHERE Id IN :myWOIds];
            for (WorkOrder wo : worders) {
                if (wo.Opportunity__c != null) {
                    oppIds.add(wo.Opportunity__c);
                }
                if (wo.Lead__c != null) {
                    woIdandLeadId.put(wo.Id, wo.Lead__c);
                    leadIds.add(wo.Lead__c);
                }
            }
        }
        
        
        System.debug('OVIDIUUUUUU myWOIDs size' + myWOIds.size());
        
        
        // two cases for Opportunities and Leads linked to the SAs
        if (oppIds.size() > 0) {
            oppSAs = [SELECT Id, Status, Opportunity__c FROM ServiceAppointment WHERE Opportunity__c IN :oppIds AND
                                            Status IN ('Scheduled', 'Dispatched', 'In Progress') AND Id NOT IN :mySAIds];
            System.debug('OVIDIUUUUUU OPPSAs size' + oppSAs.size());
            for (ServiceAppointment sa1 : oppSAs) {
                sa1.Status = 'Completed';
            }
            try {
                update oppSAs;
            } catch (DMLException e) {
                System.debug('Update SAs status in trigger CompleteAllOtherSATrigger' + e.getMessage());
            }
        }
     
        if (leadIds.size() > 0) {
            leadSAs = [SELECT Id, Status, Lead__c FROM ServiceAppointment WHERE Lead__c IN :leadIds AND
                                        Status IN ('Scheduled', 'Dispatched', 'In Progress') AND Id NOT IN :mySAIds];
            System.debug('OVIDIUUUUUU leads size' + leadIds.size());
     
            for (ServiceAppointment sa2 : leadSAs) {
                sa2.Status = 'Completed';
            }
             try {
                update leadSAs;
            } catch (DMLException e) {
                System.debug('Update SAs status in trigger CompleteAllOtherSATrigger' + e.getMessage());
            }  
        }
        
        // edited this process builder Service Appointment Insert/ Edit 
        //  fac un field pe opportunity si pe lead, pe lead il pun aici. pe opp il pun in convert method-> convert triggerd by SA id si il bag acolo
        //after canceling the SAs, we will check if we had Final Appointments and convert the according leads
        /*List<Id> leadsToConvertIds = new List<Id>();
        Map<Id, Id> WoAndSaMap = new Map<Id,Id>();
        Map<Id, Id> LeadAndWOMap = new Map<Id,Id>();
        for (ServiceAppointment sa : Trigger.new) {
            System.debug('OVIDIUUUUUUUUUU SA parent:' + sa.ParentRecordId);
            if(sa.ParentRecordId != null) {
                WoAndSaMap.put(sa.ParentRecordId, sa.Id);
            }
        }
        for (WorkOrder wo : [SELECT Id, Lead__c, WorkTypeId FROM WorkOrder WHERE Id IN : WoAndSaMap.keySet()]) {
            System.debug('OVIDIUUUUUUUUUU WO Id:' + wo.Id);
            System.debug('OVIDIUUUUUUUUUU WO Lead:' + wo.Lead__c);
            System.debug('OVIDIUUUUUUUUUU SA work type:' + wo.WorkTypeId);
            
            if (wo.Lead__c != null && wo.WorkTypeId == '08q290000008Z8CAAU') { 
                leadsToConvertIds.add(wo.Lead__c);
                LeadAndWoMap.put(wo.Lead__c, wo.Id);
            }
        }
        
        System.debug('OVIDIUUUUUUUUUU Leads to convert Ids:' + leadsToConvertIds.size());
        List<Lead> leadsToConvert = [SELECT Id, Name, FirstName, LastName, Email, Number_of_Family_Members__c, Status,
                  Household_Income__c, kW_Used__c, Finance_Recommendation__c, Not_Interested__c,
                  Not_Qualified_Reason__c, Start_Conversion__c, isConverted, HasProposalApp__c,
                  Street, Homeowner_2_Email__c, Homeowner_2_First_Name__c, Country, MobilePhone,
                  Alternate_Email__c, Homeowner_2_Alternate_Email__c, City, State, PostalCode,
                  Homeowner_2_Last_Name__c, Homeowner_2_Phone__c, Disqualified_Reason__c, SATriggConvert__c
                  from Lead where Id IN :leadsToConvertIds];
        System.debug('OVIDIUUUUUUUUUU Leads to convert:' + leadsToConvert.size());
        for (Lead l : leadsToConvert) {
            l.HasProposalApp__c = true;
            l.SATriggConvert__c = WoAndSaMap.get(LeadAndWoMap.get(l.Id));
        }
        
        try {
            update leadsToConvert;
        } catch (DMLException e) {
            System.debug('Lead Update Failed in CompleteAllOtherSATriggers' + e.getMessage());
        }*/
    }
}